<VirtualHost *:80>
   ServerName local.neoauto3.com
   ServerAdmin joejansanchez@gmail.com

   SetEnv APPLICATION_ENV local
   DocumentRoot /home/frontend/htdocs/neoauto2/public/neoauto3/public
   <Directory />
      Options FollowSymLinks
      AllowOverride None
   </Directory>
   <Directory /home/frontend/htdocs/neoauto2/public/neoauto3/public/>
      Options Indexes FollowSymLinks MultiViews
      AllowOverride All
      Order allow,deny
      allow from all
      Require all granted
   </Directory>

   ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
   <Directory "/usr/lib/cgi-bin">
      AllowOverride None
      Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
      Order allow,deny
      Allow from all
   </Directory>

   ErrorLog ${APACHE_LOG_DIR}/error-local.neoauto3.com.log

   # Possible values include: debug, info, notice, warn, error, crit,
   # alert, emerg.
   LogLevel warn

   CustomLog ${APACHE_LOG_DIR}/access-local.neoauto3.com.log combined
</VirtualHost>
