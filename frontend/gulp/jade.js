
var locals = {},
	time = new Date().getTime();

locals.version = time;

/*
* Exports locals
*/
module.exports = locals;
