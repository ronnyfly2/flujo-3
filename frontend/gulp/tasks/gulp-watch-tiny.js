/*!!
*
* Tareas para solo compilar jade rapidamente.
* coffee y stylus se mantienen el watch como antes.
* 
* tarea principal: gulp watch tiny
*/

function Task(gulp, path, options, plugins, settings, callback) {

    var gutil = require('gulp-util');
    var browserSync = settings.browserSync;
    var reload = browserSync.reload;
    var coffeeTasks = ['js', reload],        
        stylusTasks = ['styles', reload];
    
    
    gulp.task('server', function () {
        return browserSync(options.browserSync);
    });

    gulp.task('watch:tiny', function () {        
        gulp.start('server');
        gulp.watch(path.watch.jade, callback);
        gulp.watch(path.watch.coffee, coffeeTasks);
        gulp.watch(path.watch.stylus, stylusTasks);
        
    }) 
}

module.exports = Task;
