
function Task(gulp, path, options, plugins, settings, file) {
    
    var browserSync = settings.browserSync,
        module = options.getNameModule(file),        
        ruteSpecific = "modules/" + module,
        rutePath = path.src.jade + ruteSpecific + '/**/*.jade',
        baseDir = path.src.jade + ruteSpecific,
        rutePathDest = path.jade.backend.dest + ruteSpecific;

    if( module == 'all'){
        rutePath = path.jade.backend.src;
        baseDir = path.src.jade;
        rutePathDest = path.jade.backend.dest;
    }   
    
    browserSync.notify("Compiling, please wait!");    

    gulp.src(rutePath)
    .pipe(plugins.plumber())
    .pipe(settings.jadeInheritance({basedir: baseDir}))
    .pipe(plugins.jade(options.jade.backend))
    .on("error", settings.notify.onError(options.jade.notify.error))
    .on("error", console.log)
    .pipe(plugins.rename(options.jade.backend.rename))
    .pipe(gulp.dest(rutePathDest))
    .pipe(settings.notify(options.jade.notify.end))

    browserSync.watch(rutePathDest).on("change", browserSync.reload)    
    
}

module.exports = Task;