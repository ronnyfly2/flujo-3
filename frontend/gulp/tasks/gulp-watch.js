/*!!
*
* Tareas para correr un servidor local con browserSync
*
* tarea principal: gulp watch
*/

function Task(gulp, path, options, plugins, settings, callback) {

    var gutil = require('gulp-util');
    var browserSync = settings.browserSync;
    var reload = browserSync.reload;
    var coffeeTasks = ['js', reload],
        jadeTasks = ['html:backend', reload],       
        stylusTasks = ['styles', reload];

    gulp.task('watch', function () {     
        gulp.watch(path.watch.jade, jadeTasks);
        gulp.watch(path.watch.coffee, coffeeTasks);
        gulp.watch(path.watch.stylus, stylusTasks);
    });
    
}

module.exports = Task;
