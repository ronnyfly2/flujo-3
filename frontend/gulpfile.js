/*!!
 *
 * gulpfile.js
 * @author: Jan Sanchez, Vistor Sandoval
 *
 */
var gulp    = require('gulp'),
    path    = require('./gulp/path'),
    options = require('./gulp/options'),
    settings = {
        browserSync : require('browser-sync'),
        notifier    : require("node-notifier"),
        notify      : require("gulp-notify"),        
        changelog   : require('conventional-changelog'),
        jadeInheritance : require('gulp-jade-inheritance'),        
        fs          : require('fs'),
        gutil       : require('gulp-util'),
        loadPlugins : require('gulp-load-plugins'),
        package     : require('./package.json'),
        config      : require('./gulp/config.local')
    },
    plugins = settings.loadPlugins(),  
    callback = function(file){
        var Task = require("./gulp/tasks/gulp-callback");
        return new Task(gulp, path, options, plugins, settings, file);
    },    
    runTask = function (nameTask){
        var Task = require("./gulp/tasks/" + nameTask);
        return new Task(gulp, path, options, plugins, settings, callback);
    };

plugins.minifyCSS = require('gulp-minify-css');
plugins.runSequence = require('run-sequence');
plugins.spritesmith = require('gulp.spritesmith');

plugins.es = require('event-stream');
plugins.Buffer = require('buffer').Buffer;

runTask("gulp-clean");
runTask("gulp-backend");
runTask("gulp-copy");
runTask("gulp-styles");
runTask("gulp-fonts");
runTask("gulp-html");
runTask("gulp-icons");
runTask("gulp-js");
runTask("gulp-sprites");
runTask("gulp-watch-tiny");
runTask("gulp-version");

/*!!
*
* Tareas por default
*
* tarea principal: gulp
*/

gulp.task('default', ['clean'], function (cb) {
    plugins.runSequence('html', 'sprites', 'icons', 'fonts', 'js:default', 'copy','styles', cb);
});
