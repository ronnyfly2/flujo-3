yOSON.AppSchema.modules =
	"home":
		"controllers":
			"index":
				"allActions": ()->
					return
				"actions":
					"index": ()->
						yOSON.AppCore.runModule "search"
						yOSON.AppCore.runModule "search_selects"
						yOSON.AppCore.runModule "search_inputs"
						yOSON.AppCore.runModule "slider_blog"
						yOSON.AppCore.runModule "sticky"
						yOSON.AppCore.runModule "suscribe_newsletter"
						yOSON.AppCore.runModule "banner"
						yOSON.AppCore.runModule "touch_menu_principal"						
						yOSON.AppCore.runModule "valid_year", parent : ".form_vehicle"

						yOSON.AppCore.runModule "tracking_links", content: ".ads_featured", link: "li .track", 		params : ["vehiculos_destacados", "home"]
						yOSON.AppCore.runModule "tracking_links", content: ".search_by", 	link: "a.track", 		params : ["buscar", "buscar_por"]
						yOSON.AppCore.runModule "tracking_links", content: ".ads_featured", link: ".btn.track", 	params : ["mas_avisos", "home"]
						yOSON.AppCore.runModule "tracking_links", content: ".info", 		link: ".track", 		params : ["banner_revendedor", "home"]
						yOSON.AppCore.runModule "tracking_links", content: ".social_networks.home", link: ".track", params : ["compartir", "home_blog"]
						yOSON.AppCore.runModule "tracking_links", content: ".search_advanced", link: ".sell_vehicle.track" , params : ["vender", "portada"]

						yOSON.AppCore.runModule "tracking_form", content: ".search_advanced", form: ".form_vehicle"
						yOSON.AppCore.runModule "tracking_form", content: ".stickyfloat", form: ".form_sticky"
						return
					"byDefault": () ->
						return
			"byDefault": () ->
				return
		"byDefault": () ->
				return
		"allControllers": () ->
			return
	"auth":
		"controllers":
			"register":
				"allActions": ()->
					return
				"actions":
					"index": ()->					
						return
					"register": ()->
						yOSON.AppCore.runModule "switchery"
						yOSON.AppCore.runModule "register"					
						return
					"byDefault": () ->
						return
			"byDefault": () ->
				return
		"byDefault": () ->
				return
		"allControllers": () ->
			return
	"ficha-aviso":
		"controllers":
			"index":
				"allActions": ()->
					yOSON.AppCore.runModule "zoom_visor"
					yOSON.AppCore.runModule "effect_visor", effect: "fade"
					yOSON.AppCore.runModule "visor_thumbnail"
					yOSON.AppCore.runModule "carrusel_images"
					yOSON.AppCore.runModule "slider_thumbnail"
					yOSON.AppCore.runModule "slider_masks"
					yOSON.AppCore.runModule "underscore"
					yOSON.AppCore.runModule "fancybox"
					yOSON.AppCore.runModule "reportar_aviso"
					yOSON.AppCore.runModule "enviar_amigo"
					yOSON.AppCore.runModule "login"
					yOSON.AppCore.runModule "recaptcha"
					# yOSON.AppCore.runModule "collapser"
					yOSON.AppCore.runModule "contact_advertiser"
					return
				"actions":
					"ficha-nuevo": ()->			
						yOSON.AppCore.runModule "features_nuevo"
						return
					"byDefault": () ->
						return
			"byDefault": () ->
				return
		"byDefault": () ->
				return
		"allControllers": () ->
			return
	"search":
		"controllers":
			"index":
				"allActions": ()->					
					yOSON.AppCore.runModule "search_filters"
					yOSON.AppCore.runModule "modal_login"
					yOSON.AppCore.runModule "save_search"
					yOSON.AppCore.runModule "switchery"
					return
				"actions":
					"index": ()->					
						return
					"search": ()->
						yOSON.AppCore.runModule "carrusel_images"
						yOSON.AppCore.runModule "loader_slider_images"
						yOSON.AppCore.runModule "init_ellipsis"						
						yOSON.AppCore.runModule "valid_year"
						#yOSON.AppCore.runModule "slider_filters"

						#yOSON.AppCore.runModule "persistence_redirect_search_results"
						yOSON.AppCore.runModule "order_by"						
						return	
					"byDefault": () ->
						return
			"byDefault": () ->
				return
		"byDefault": () ->
				return
		"allControllers": () ->
			return	

	"anunciante":
		"controllers":
			"publicacion":
				"allActions": ()->
					return
				"actions":
					"datos-aviso": ()->
						yOSON.AppCore.runModule "get_models_by_brand"
						yOSON.AppCore.runModule "character_count"
						yOSON.AppCore.runModule "get_districts_by_departament"
						yOSON.AppCore.runModule "switchery", switchCheckbox : ".switch"
						yOSON.AppCore.runModule "get_suggested_price"
						yOSON.AppCore.runModule "sortableDrop"
						yOSON.AppCore.runModule "fileuploader"
						return
					"byDefault": () ->
						return
			"byDefault": () ->
				return
		"byDefault": () ->
				return
		"allControllers": () ->
			return
	"sitemap":
		"controllers":
			"index":
				"allActions": ()->
					#yOSON.AppCore.runModule "example"
					return
				"actions":
					"index": ()->
						yOSON.AppCore.runModule "alphabet_sitemap"
						return
					#"sitemap": ()->
					#   return  
					"byDefault": () ->
						return
			"byDefault": () ->
				return
		"byDefault": () ->
				return
		"allControllers": () ->
			return  
	"byDefault": () ->
		return
	"allModules": () ->		
		yOSON.AppCore.runModule "tracking"				
		#yOSON.AppCore.runModule "touch_close_element"		
		yOSON.AppCore.runModule "add_msie"
		yOSON.AppCore.runModule "search_top"
		yOSON.AppCore.runModule "mobile_menu"
		yOSON.AppCore.runModule "lazyload"
		yOSON.AppCore.runModule "neo_tabs"
		yOSON.AppCore.runModule "reloadSelectivizr"
		yOSON.AppCore.runModule "placeholder"
		yOSON.AppCore.runModule "pretty_checkbox"
		#yOSON.AppCore.runModule "modal_login"
		yOSON.AppCore.runModule "eplanning"
		yOSON.AppCore.runModule "show_popup"		
		yOSON.AppCore.runModule "logo_svg"		
		yOSON.AppCore.runModule "tracking_links", content: ".login_link", 		link: ".track", params : ["logueo", "header"]
		yOSON.AppCore.runModule "tracking_links", content: ".register_link", 	link: ".track", params : ["registro", "header"]
		yOSON.AppCore.runModule "tracking_links", content: ".link_externals", 	link: ".track", params : ["menu_header", "menu_header"]
		yOSON.AppCore.runModule "tracking_links", content: ".links_menu", 		link: ".track", params : ["servicios"]
		yOSON.AppCore.runModule "tracking_links", content: ".left_footer", 		link: ".track", params : ["opcion_footer", "footer"]
		yOSON.AppCore.runModule "tracking_links", content: ".central_help", 	link: ".track", params : ["central_ayuda", "central_ayuda"]
		yOSON.AppCore.runModule "tracking_links", content: ".social_networks.footer", 	link: ".track", params : ["compartir", "footer"]
		yOSON.AppCore.runModule "tracking_links", content: ".box_call_free", 	link: ".track", params : ["llamar", "call_center"]
		return