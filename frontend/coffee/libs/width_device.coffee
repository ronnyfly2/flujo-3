###
Detecta el ancho del viewport
@class width_device
@main libs
@author Jhonnatan Castro 
###

yOSON.AppCore.addModule "width_device", (Sb) ->
    defaults = {     
        el          : "window"
        widthMobile : 670
    }
    st = {}
    dom = {}
    isMobile = false

    catchDom = () ->        
        dom.window    = $(st.el)
        return
    suscribeEvents = () ->
        dom.window.on('resize', events.resize)
        return

    events = {
        resize : (e)->
        	deviceWidth = e.target.outerWidth
        	if deviceWidth <= st.widthMobile
        		isMobile = true
        	else
        		isMobile = false
        	return

        isViewportMobile : ->
        	isMobile        
    }
    fn = {
        functionName : () ->
            log("example")
            return
    }
    initialize = (opts) ->
        st = $.extend({}, defaults, opts)
        catchDom(st)
        suscribeEvents()
        Sb.events(["isViewportMobile"], events.isViewportMobile, this)
        return

    return {
        init: initialize
    }
,[]