###
Module description
@class demo
@main neoauto3/coffee/scripts/modules/home/index
@author Ronny Cabrera
###

yOSON.AppCore.addModule "alphabet_sitemap", (Sb) ->
    defaults = {
        parentScroll     : ".options_contents"
        scrollTitle      : ".scroll_title"
        parent           : ".options_alphabet"
        letter           : ".letter"
        boxContentList   : ".box_content_list"
        actived          : ".active"
        listByBrandModel : ".cars_by_brand_and_model"
        tpl              : "#tplAlphabet"
        urlAjax          : yOSON.baseHost + "/ajax/common/modelos-more-published/letter/"
        urlHash          : "!/vehiculos-por-marca/"
    }
    st = {}
    dom = {}
    afterCatchDom = ()->
        _.templateSettings =
            evaluate: /\{\{([\s\S]+?)\}\}/g
            interpolate: /\{\{=([\s\S]+?)\}\}/g
        fn.afterLoading()
        fn.validateSlash()
        return
    catchDom = (st) ->
        dom.parentScroll      = $(st.parentScroll)
        dom.scrollTitle       = $(st.scrollTitle, dom.parentScroll)
        dom.parent            = $(st.parent)
        dom.letter            = $(st.letter, dom.parent)
        dom.boxContentList    = $(st.boxContentList)
        dom.actived           = $(st.actived, dom.letter)
        dom.listByBrandModel  = $(st.listByBrandModel) 
        dom.tpl               = $(st.tpl)
        # dom.cloneUls        = $('<ul class=' + '\'cars_by_brand_and_model\'' + '></ul>')
        return

    suscribeEvents = () ->
        dom.letter.on "click", events.getLetter
        return

    events = {
        getLetter : (e) ->
            ele    = $(e.target)
            letter = ele.data("letter")
            events.setLetter(letter)
            return
        setLetter : (letter) ->
            if letter != ""
                document.location.hash = st.urlHash + letter
                fn.autoScroll()
            letter = if letter == "" then "AA" else letter
            dom.letter.removeClass("active")
            $(".letter_"+letter).addClass("active")
            events.getAjax(letter)
            return
        getAjax : (letter) ->
            ajaxget = $.ajax
                url      : st.urlAjax + letter
                type     : "GET"
                dataType : "json"
                success  : (result)->
                    fn.createLists(result)
                    return
            return
    }
    fn = {
        createLists : (result, e)->
            if result.length > 0
                template = _.template($("#tplAlphabet").html(),{"data":result})
                dom.boxContentList.html(template)
            else
                dom.boxContentList.html("No tiene data")

            typeBrowser = navigator.userAgent
            if typeBrowser.indexOf('MSIE 8') >= 0 || typeBrowser.indexOf('MSIE 9') >= 0
                fn.divideListsIe()
            else
                fn.divideLists()
            return
        autoScroll : () ->
            # setTimeout (->
            #   $("body").animate({scrollTop: dom.scrollTitle.offset().top }, 1000)
            #   return
            # ), 400
            $("body").animate({scrollTop: dom.scrollTitle.offset().top }, 1000)
            return
        divideListsIe : () ->
            catchDom(st)
            listsUls = dom.listByBrandModel
            dom.listByBrandModel.remove()
            # listsUls = $(".cars_by_brand_and_model")
            # $(".cars_by_brand_and_model").remove()
            while (group = listsUls.find('li:lt(12)').remove()).length
                $("<ul>").addClass("cars_by_brand_and_model").append(group).appendTo '.box_content_list'
            return  
        afterLoading : ()->
            $(()->
                hashLinks = document.location.hash
                letter = hashLinks.replace("#" + st.urlHash, "")
                events.setLetter(letter)
                return
            )
        validateSlash : ()->
            if document.location.pathname.search(/\/$/) == -1
                document.location.pathname = document.location.pathname + "/"

            return
        divideLists: ()->
            container = dom.boxContentList
            items = container.find("li")
            # clean container
            container.html ""
            total = items.length
            columns = 4
            arbol = splitColumns(total, columns)
            i = 0
            while i < arbol.length
                listaUl = $("<ul>").addClass("cars_by_brand_and_model")
                j = 0
                while j < arbol[i].length
                    crasdd = listaUl.append items.eq(arbol[i][j])
                    j++
                container.append listaUl
                i++
            return
        }
    initialize = (opts) ->
        st = $.extend({}, defaults, opts)
        catchDom(st)
        afterCatchDom()
        suscribeEvents()
        return
    return {
        init: initialize
    }
,["js/dist/libs/underscore/underscore.js"]