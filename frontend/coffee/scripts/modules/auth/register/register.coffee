###
Registro
@class register
@main neoauto/auth/register
@author Juan Pablo Chullo
###
yOSON.AppCore.addModule "register", (Sb) ->
  defaults = {
    parent             : ".form_register",
    parentPassword     : ".c_password"
    showPassword       : "#showPassword"
  }
  st = {}
  dom = {}
  
  catchDom = () ->
    dom.parent              = $(st.parent)
    dom.parentPassword      = $(st.parentPassword,st.parent)
    dom.showPassword        = $(st.showPassword, dom.parentPassword)
    return
  afterCatchDom = () ->
    # init validation to form register
    dom.parent.parsley()
    return
  suscribeEvents = () ->
    dom.showPassword.on('change', events.showPassword)
    return
  events = {
    showPassword: (e) ->
      if($(@).is(":checked"))
        $(@).closest(st.parentPassword).find('input[name=contrasenia]').attr('type','text')
      else
        $(@).closest(st.parentPassword).find('input[name=contrasenia]').attr('type','password')
      return
  }
  fn = {
    setParsleyLanguage : (lang) ->
      window.ParsleyValidator.setLocale(lang)
      return
  }
  initialize = (opts) ->
    st = $.extend({}, defaults, opts)
    catchDom()
    afterCatchDom()
    suscribeEvents()
    fn.setParsleyLanguage('es')
    log "register"
    return

  return {
    init: initialize
  }
,['js/dist/libs/parsleyjs/src/i18n/es.js','js/dist/libs/parsleyjs/dist/parsley.min.js']