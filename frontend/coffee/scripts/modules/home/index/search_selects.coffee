###
Module description
@class search
@main home/index
@author Juan Pablo Chullo
###

yOSON.AppCore.addModule "search_selects", (Sb) ->
	defaults =
		parent				: ".search_advanced"
		selectBrand 		: ".select_brand"
		selectModel 		: ".select_model"
		loader 				: ".loader"
		urlAjax : 
			setVehicleBrand : yOSON.baseHost + "/ajax/common/marcas-by-tipo-vehiculo/slugTipoVehiculo/"
			setVehicleModel	: yOSON.baseHost + "/ajax/common/modelos-by-marca/slugMarca/"
		type 				: "autos"

	st = {}
	dom = {}
	
	# init vehicle data TMP
	yOSON.tmp.vehicles = { type:{autos:{}, motos:{} } }

	# titulos comboBox
	titleBrand = "Escoge Tu Marca"
	titleModel = "Escoge El Modelo"

	catchDom = () ->
		dom.parent 		= $(st.parent)

		#selects
		dom.selectBrand = $(st.selectBrand, dom.parent)
		dom.selectModel = $(st.selectModel, dom.parent)

		#loader
		dom.loader 		= $(st.loader, dom.parent)
		dom.loaderBrand = dom.selectBrand.prev(st.loader)
		dom.loaderModel = dom.selectModel.prev(st.loader)
		return

	suscribeEvents = () ->
		dom.selectBrand.on 	"change", 	events.prepareAjaxVehicleModel
		return

	events = {
		# events
		prepareAjaxVehicleModel: (e) ->
			brand = $(@).val()
			fn.ajaxVehicleModel(brand)
			return
	}
	fn = {
		# 
		ajaxVehicleModel: (brand) ->
			window.neo_ajax
					url : 	st.urlAjax.setVehicleModel+brand+"/slugTipoVehiculo/"+st.type
					type:	"json"
				,
				dom.loaderModel,
				fn.ajaxCallbackModel
			return
		ajaxCallbackModel:(result) ->
			dom.selectModel.html(fn.createOptionSelectBox(result, titleModel))
			return
		# 
		loadVehicleByType: (type) ->
			# set type
			st.type = type
			vehicles = fn.getDataVehiclesTmp()
			dom.selectBrand.html(fn.createOptionSelectBox(vehicles, titleBrand))
			fn.cleanSelectBox(dom.selectModel, titleModel)
			return	
		getDataVehiclesTmp: () ->
			return yOSON.tmp[st.type]

		# html
		createOptionSelectBox: (arrs_json, titleOption) ->
			items = ["<option value=''>"+titleOption+"</option>"]
			$.each arrs_json, (i,json) ->
				items.push("<option value='"+json.key+"'>"+capitalize(json.value)+"</option>")
				return
			return items.join('')
		cleanSelectBox: (element, titleOption) ->
			element.html("<option value=''>"+titleOption+"</option>")
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		Sb.events(['loadSelectByType'], fn.loadVehicleByType, this)
		# cargar por defecto auto
		fn.loadVehicleByType(st.type)
		log 'script search_selects'
		return

	return {
		init: initialize
	}
,[]