###
Añade la funcionalidad slider para la seccion blog de neoauto
@class slider_blog
@main home/index
@author Jhonnatan Castro Vilela
###

yOSON.AppCore.addModule "slider_blog", (Sb) ->
	defaults = {
		parent      : ".blog"
		el          : ".slider_blog ul"
		figure	 	: "figure"
		desktop : 
			breakpoint: 1440
			settings: 
				slidesToShow 	: 3
				slidesToScroll 	: 1
				infinite 		: true
		tablet : 
			breakpoint: 700
			settings: 
				slidesToShow 	: 2
				slidesToScroll 	: 1
				infinite 		: true
	}
	st = {}
	dom = {}
	countImgLoaded = 0
	images = []

	catchDom = () ->
		dom.parent 	= $(st.parent)
		dom.el     	= $(st.el, dom.parent)
		dom.li 		= dom.el.find("li")
		dom.figure 	= $(st.figure, dom.el)
		return

	afterCatchDom = ()->			
		fn.beforeLoadSlick()		
		return		


	fn = {
		
		setByDefaultSlick : ->
			dom.el.removeClass("init")
			dom.figure.removeClass("init")
			return

		beforeLoadSlick : ->
			fn.prepareImages()
			
			beforeLoadImages
				images : images
				MaxNumberImagesLoad : st.desktop.settings.slidesToShow + 1
				callback : fn.afterLoadImages
			return

		prepareImages : ->
			dom.li.each( (index, ele)->
				if index <= st.desktop.settings.slidesToShow
					images.push($(ele).find("img").attr("src"))
				return
			)
			return		

		afterLoadImages : ->
			fn.setByDefaultSlick()
			fn.loadSlick()
			fn.showSlick()
			return

		showSlick : ->
			dom.el.css("opacity",1)
			return

		loadSlick : ->
			if dom.li.length > 3
				dom.li.css("margin","0 .2%")
				dom.el.slick
					speed 		: 300
					infinite 	: false
					slidesToShow: 3
					slidesToScroll: 3
					responsive : [st.desktop, st.tablet]
			return
	}

	
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)		
		catchDom()
		afterCatchDom()
		return

	return {
		init: initialize
	}
,["js/dist/libs/slick-carousel/slick/slick.min.js"]