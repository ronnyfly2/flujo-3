###
Module description
@class search
@main home/index
@author Juan Pablo Chullo
###

yOSON.AppCore.addModule "search_inputs", (Sb) ->
	defaults =
		parent				: ".search_advanced"
		priceInput 			: ".priceInput"
		anioInput 			: ".anioInput"
		anioDesde 			: "#anioDesde"
		anioHasta 			: "#anioHasta"
		anioFinal 			: "#anioFinal"
	st = {}
	dom = {}
	d = 0
	catchDom = () ->
		dom.parent 		= $(st.parent)
		#anio
		dom.priceInput 	= $(st.priceInput, dom.parent)
		dom.anioInput 	= $(st.anioInput, dom.parent)
		dom.anioDesde 	= $(st.anioDesde, dom.parent)
		dom.anioHasta 	= $(st.anioHasta, dom.parent)
		dom.anioFinal 	= $(st.anioFinal, dom.parent)
		return

	afterCatchDom = ->
		fn.registerToParsley()
		return

	suscribeEvents = () ->
		# dom.anioInput.on 	"keyup", 	events.inputKeyboardAnio
		# dom.priceInput.on 	"keyup", 	events.inputKeyboardPrice
		# $.listen('parsley:field:error', events.callbackErrorParsley)
		
		return

	events = {
		# events
		inputKeyboardAnio: (e) ->
			fn.prepareInputAnio()
			fn.cleanInputsNumber($(@))
			return
		inputKeyboardPrice: (e) ->
			fn.cleanInputsNumber($(@))
			return
		callbackErrorParsley: (e) ->
			log e
			window.miele = e
			setTimeout () ->
				e.$element.next("ul").find('li').prepend("<i class='icon icon_alert_error'></i>");
				return
			,200
	}
	fn = {
		prepareInputAnio: () ->
			textAnioDesde = valid.isEmpty( dom.anioDesde.val(), dom.anioDesde.data('min') )
			textAnioHasta = valid.isEmpty( dom.anioHasta.val(), dom.anioHasta.data('max') )
			
			if(dom.anioDesde.val() < dom.anioDesde.data('min') || dom.anioDesde.val() > dom.anioDesde.data('max'))
				textAnioDesde = dom.anioDesde.data('min')

			if(dom.anioHasta.val() > dom.anioHasta.data('max') || dom.anioHasta.val() < dom.anioHasta.data('min'))
				textAnioHasta = dom.anioHasta.data('max')

			dom.anioFinal.val(textAnioHasta + '-' + textAnioDesde)
			log("year",dom.anioFinal.val())
			return
		cleanInputsNumber: (self) ->
			textSelf 	  = self.val()
			# si el texto no es numero simplemente limpia
			if(!valid.isNumber(textSelf))
				self.val( textSelf.substring(0, -1) )
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		#afterCatchDom()
		suscribeEvents()
		log 'script search_inputs'
		return

	return {
		init: initialize
	}
,['js/dist/libs/parsleyjs/dist/parsley.min.js']