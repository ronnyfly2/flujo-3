###
Module description
@class sticky
@main modules/home/index
@author Jhonnatan Castro
###

yOSON.AppCore.addModule "sticky", (Sb) ->
	defaults = {
		parent      : "body"
		el          : ".stickyfloat"
		foot 		: ".blog"
		margin 		: 10
		posicionInicial : 0
	}
	st = {}
	dom = {}

	catchDom = (st) ->
		dom.parent = $(st.parent)
		dom.el     = $(st.el, dom.parent)
		dom.foot   = $(st.foot)
		return

	afterCatchDom = ()->
		fn.ubicarPosicionInicial()
		return
	
	suscribeEvents = () ->
		$(window).on('scroll', events.moveStick)
		return

	events = {
		moveStick : (e) ->
			windowpos = $(window).scrollTop()
			box = dom.el
			foot = dom.foot.offset()
			if ($(window).height() < foot.top - (windowpos + st.margin))
				pos = windowpos + $(window).height() - (box.height() + st.margin)
				dom.el.css(
					top: pos + "px"
					bottom: ''
				)
			else
				pos = foot.top - (box.height() + st.margin)
				dom.el.css(
					top: pos + "px"
					bottom: ''
				)
			return
	}
	fn = {
		ubicarPosicionInicial : () ->
			newPosition = $(window).height() - dom.el.height() - st.margin;
			$(st.el).css('top', newPosition + "px");
			posicionInicial = newPosition;
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		afterCatchDom()
		suscribeEvents()
		Sb.events(["moveStick"],events.moveStick,this)
		return

	return {
		init: initialize
	}
,[]