###
Module description
@class search
@main home/index
@author Juan Pablo Chullo
###
yOSON.AppCore.addModule "search", (Sb) ->
	defaults =
		parent				: ".search_advanced"
		tabOption			: ".tab_option a"
		tabContent			: ".tab_content form"
		#formVehicle			: ".form_vehicle"
		tipoVehicle 		: "#tipoVehicle"
	st = {}
	dom = {}
	eventListener = ""

	catchDom = () ->
		dom.parent 		= $(st.parent)
		#form
		#dom.formVehicle = $(st.formVehicle)
		#tabs
		dom.tabOption 	= $(st.tabOption, dom.parent)
		dom.tabContent 	= $(st.tabContent, dom.parent)
		#type
		dom.typeVehicleHidden = $(st.tipoVehicle, dom.parent)
		return
	afterCatchDom = () ->
		# init validator
		#dom.formVehicle.parsley()
		fn.validateTouchDevice()
		return
	suscribeEvents = () ->
		dom.tabOption.on(eventListener,events.tabOption)
		return

	events = {
		# events
		# setear datos provenientes del click
		tabOption : (e) ->
			e.preventDefault()
			fn.prepareTab($(@))
			return
	}
	fn = {
		prepareTab: (self) ->
			# get (autos|motos)
			typeVehicle = self.attr('option')

			dom.tabOption.removeClass('active')
			self.addClass('active')

			# @parameters: (autos|motos) 
			dom.typeVehicleHidden.val(typeVehicle)

			# @parameters: (autos|motos) => seach_selects.js
			Sb.trigger("loadSelectByType", typeVehicle)
			
			fn.changeTitleButtonVehicle(typeVehicle)

			dom.parent.find('input[type=radio]').focus()
			return

		changeTitleButtonVehicle : (type) ->
			b = $(".sell_vehicle")
			if(type=="autos")
				b.find('i').attr('class','icon icon_car_one')
				b.find('span').text('VENDE TU AUTO')
			else
				b.find('i').attr('class','icon icon_moto')
				b.find('span').text('VENDE TU MOTO')
			return
		validateTouchDevice : ->
			if isMobile.any()
				dom.tabOption.each(fn.listOptions)
				eventListener = "touchend"
			else
				eventListener = "click"
			return
		
		listOptions : (index, option)->
			Sb.trigger("setChangeCursorPointer", option)
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		afterCatchDom()
		suscribeEvents()
		log 'script search'
		return

	return {
		init: initialize
	}
,['js/dist/libs/parsleyjs/dist/parsley.min.js']