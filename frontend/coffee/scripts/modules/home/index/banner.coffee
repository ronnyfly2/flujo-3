###
Modulo para generar la precarga optima del banner
@class banner
@main home/index
@author 
###

yOSON.AppCore.addModule "banner", (Sb) ->
	
	defaults = {
		el  : ".banner_home"		
	}

	st = {}
	dom = {}

	catchDom = () ->		
		dom.el 		= $(st.el)
		dom.source 	= dom.el.data("src")
		return

	afterCatchDom = ->		
		fn.createImageDinamic()
		return

	fn = {

		createImageDinamic : ->
			image = $("<img>")
			image.attr('src', fn.ieCache(dom.source))
			image.hide()
			image.on('load', fn.loadImage)
			image.on('complete', fn.loadImageComplete)
			# image = new Image()
			# image.onload = fn.loadImage
			# image.src = dom.source
			# image.style.display = 'none'
			dom.el.append(image)	

			return

		loadImage : (e)->
			log e
			log "onload"
			img = $(e.target)
			img.fadeIn()
			fn.centerImage(img)
			if isMobile.Android() || isMobile.iOS()
				log "no delete"
			else
				fn.prepareBackground(img.attr('src'))
				fn.removeImage(img)
			return
		loadImageComplete : (e) ->
			log "complete"
			return
		centerImage: (img) ->
			left = ( img.width() - $(window).width() ) / 2
			img.css('left', "-" + left + "px")
			return
		removeImage: (img) ->
			img.remove()
			return
		prepareBackground: (src) ->
			dom.el
				.css('backgroundImage', 'url("'+src+'")')
				.removeAttr("data-src")
			return
		ieCache: (url) ->
			if $.browser.msie
				d = new Date()
				r = url + '?' + d.getTime()
			else
				r = url
			return r
	}

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()	
		afterCatchDom()			
		return

	return {
		init: initialize
	}