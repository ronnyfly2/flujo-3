###
Module description
@class suscribe_newsletter
@main modules/home/index
@author Jhonnatan Castro
###

yOSON.AppCore.addModule "suscribe_newsletter", (Sb) ->
	ajax = ""
	defaults = {
		content     : ".stickyfloat"
		txtMail		: ".txt_mail"		
		btnSuscribe	: ".bar"
		iconSticky	: ".icon_sticky_mail"
		loader		: ".loader"		
		form 		: "form"
		txtToken	: ".txt_token"
		btnClose 	: ".icon_close"
		boxMessage 	: ".message_sticky"
		labelChecked: "label[for=chk_autorize]"
		urlToken 	: yOSON.baseHost + "/ajax/common/get-token/"

	}

	st = {}
	dom = {}
	catchDom = () ->
		dom.content		= $(st.content)
		dom.btnSuscribe	= $(st.btnSuscribe, dom.content)
		dom.txtMail 	= $(st.txtMail, dom.content)
		dom.iconSticky 	= $(st.iconSticky, dom.content)
		dom.loader		= $(st.loader, dom.content)
		dom.form 		= $(st.form, dom.content)
		dom.txtToken	= $(st.txtToken, dom.form)
		dom.btnClose 	= $(st.btnClose, dom.content)
		dom.boxMessage 	= $(st.boxMessage, dom.content)
		dom.labelChecked= $(st.labelChecked, dom.form)
		dom.checkbox 	= dom.form.find("input[type=checkbox]")
		return
	suscribeEvents = () ->
		dom.btnSuscribe.on "click", events.showSuscribe
		dom.btnClose.on "click", events.showSuscribe		
		$.listen('parsley:field:validated', events.callbackErrorParsley)
		dom.form.on "submit", events.submit
		return

	events = {
		submit : (e)->
			e.preventDefault()
			if ajax
				ajax.abort()
			
			ajax = $.ajax(
				data: dom.form.serialize()
				url : dom.form.attr("action")
				beforeSend : ->
					dom.iconSticky.removeClass("icon_sticky_mail").addClass("loading")
					return
				success : (data)->
					dom.iconSticky.removeClass("loading").addClass("icon_sticky_mail")
					st.message = data.message
					events.suscribeOk()
					return
			)
			return
		suscribeOk : ->			
			setTimeout( events.refreshForm, 4000)			
			dom.content.addClass("success_ok")
			dom.form.hide()
			dom.boxMessage.text(st.message).show()
			return
			
		refreshForm : ->
			dom.boxMessage.text('').hide()			
			dom.content.removeClass("success_ok")
			dom.content.removeClass("move")
			dom.content.addClass("close")
			dom.form.show()
			dom.form.parsley().reset()


			dom.form[0].reset()
			if dom.labelChecked.hasClass("checked")
				dom.checkbox.attr("checked",true)
			else
				dom.checkbox.attr("checked",false)

			return

		callbackErrorParsley : (e)->			
			Sb.trigger("moveStick")			
			return		

		showSuscribe : (e) ->

			if dom.content.hasClass("move")							
				events.refreshForm()
			else			
				fn.getTokenAjax()
			return		
	}
	fn = {
		getTokenAjax : (e) ->
			if ajax
				ajax.abort()
			ajax = $.ajax(
				url			: st.urlToken
				type		: "GET"
				datatype 	: "json"			
				beforeSend 	: ->					
					dom.iconSticky.removeClass("icon_sticky_mail").addClass("loading")
					return
				success 	: (data) ->					
					dom.iconSticky.removeClass("loading").addClass("icon_sticky_mail")				
					dataToken = data.data
					dom.txtToken.val(dataToken)

					dom.content.addClass("move")			
					dom.content.removeClass("close")				
					dom.iconSticky.show()
					dom.loader.hide()
					dom.txtMail.focus()
					return
			)
			return
	}		

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}
,["js/dist/libs/parsleyjs/dist/parsley.min.js","js/dist/libs/parsleyjs/src/i18n/es.js"]