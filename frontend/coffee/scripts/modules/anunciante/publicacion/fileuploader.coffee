###
Module description
@class fileuploader
@main fileuploader/index/index
@author 
###

yOSON.AppCore.addModule "fileuploader", (Sb) ->
	defaults =
		content         : ".box_uploader"
		inputFileUpload : "#fileupload"
		inputFile       : ".up_input"
		fileUploaderOn  : 
			#url 	    : "/static/neoauto3/server/php/"
			url         : "http://local.neoauto3.com/anunciante/publicacion/upload-image-s3/"
			dataType    : 'json'
			autoUpload  : false
	
	st = {}

	dom = {}

	catchDom = (st) ->
		dom.content = $(st.content)
		dom.inputFile = $(st.inputFile, dom.content)
		dom.inputFileUpload = $(st.inputFileUpload, dom.content)
		return

	suscribeEvents = () ->
		dom.inputFileUpload.fileupload defaults.fileUploaderOn
		#dom.inputFileUpload.bind 'fileuploadprocess', events.getProccess
		#dom.inputFileUpload.bind 'fileuploaddrop', events.getDrop
		#dom.inputFileUpload.bind 'fileuploadchange', events.getChange
		#dom.inputFileUpload.bind 'fileuploaddestroyed', events.getDestroy
		#dom.inputFileUpload.bind 'fileuploadsend', events.getSend
		#dom.inputFileUpload.bind 'fileuploadfailed', events.getFail
		fn.getFileuploader()
		return

	events =
		getFail: (e, data)->
			$.each data.files, ->
				el = $(this)
				log 'fallo ' + el
				return
		getDrop: (e, data)->
			$.each data.files, ->
				el = $(this)
				log el
				return
		getChange: (e, data)->
			log $(data.content).data('order')
			$.each data.files, ->
				el = $(this)
				log el
				return

		getSend: (e, data)->
			$.each data.files, ->
				el = $(this)
				log el
				return

		getProccess: (e, data)->
			log 'Processing ' + data.files[data.index].name + '...'
			$.each data.files, ->
				el = $(this)
				log el
				return
			return
		getDestroy: (e, data)->
			log 'data ' + data.type

	fn =		
		getFileuploader: ()->
			#urlServer = dom.inputFileUpload.fileupload('option','url')
			urlServer = 'http://local.neoauto3.com/anunciante/publicacion/get-images-s3'
			$.ajax(
				url: urlServer
				dataType: 'json'
				context: dom.inputFileUpload[0]).done (result) ->
				$(this).fileupload('option', 'done').call this, $.Event('done'), result: result
				log urlServer
				return		

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		suscribeEvents()
		return

	return {
		init: initialize
	}
, [ 'js/dist/libs/jquery-ui/ui/widget.js',
	'js/dist/libs/blueimp-tmpl/js/tmpl.min.js',
	'js/dist/libs/blueimp-load-image/js/load-image.all.min.js',
	'js/dist/libs/blueimp-file-upload/js/jquery.iframe-transport.js',
	'js/dist/libs/blueimp-file-upload/js/jquery.fileupload.js',
	'js/dist/libs/blueimp-file-upload/js/jquery.fileupload-process.js',
	'js/dist/libs/blueimp-file-upload/js/jquery.fileupload-image.js',
	'js/dist/libs/blueimp-file-upload/js/jquery.fileupload-validate.js',
	'js/dist/libs/blueimp-file-upload/js/jquery.fileupload-ui.js',
	'js/dist/libs/blueimp-file-upload/js/jquery.fileupload-jquery-ui.js']

