###
Module description
@class get_districts_by_departament
@main anunciante/publicacion
@author Ronny Cabrera
###

yOSON.AppCore.addModule "get_districts_by_departament", (Sb) ->
	defaults = {
		parent : ".publication"
		selDepartament : ".select_departament"
		selDistrict : ".select_district"
		url : "/ajax/common/ubigeo/department/"
		textMessage : "seleciona un departamento"
	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.parent = $(st.parent)
		dom.selDepartament = $(st.selDepartament, dom.parent)
		dom.selDistrict = $(st.selDistrict, dom.parent)
		return
	suscribeEvents = () ->
		dom.selDepartament.on "change", events.selDepartaments
		return

	events = {
		selDepartaments : (e) ->
			ele   = $(e.target)
			brand = ele.val()
			progressAjax = $.ajax(
				beforeSend: fn.initLoading
				url : st.url + brand
				method: 'POST'
				success: fn.updateModel
				dataType: 'Json')
			log brand 
			return
	}

	fn = {
		createItems : (value, name) ->
			return '<option value="' + value + '">' + name + '</option>'
		updateModel : (data) ->
			fn.finishLoading()
			dom.selDistrict.html('')
			#log data.length
			#optionMsg = '<option value="">' + st.textMessage + '</option>'
			i = 0
			while i < data.length
				dom.selDistrict.append fn.createItems(data[i].IdUbigeo, data[i].NombreMostrarUbigeo)
				#log dom.selDistrict.append fn.createItems(data[i].id, data[i].name)
				#dom.selDistrict.html(optionMsg)
				i++
			dom.selDistrict.removeAttr('disabled')
			return
		initLoading : () ->
			log "estoy cargando"
			return
		finishLoading : () ->
			log "ya termine de cargar"
			return
	}

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		log 'get_districts_by_departament'
		return

	return {
		init: initialize
	}