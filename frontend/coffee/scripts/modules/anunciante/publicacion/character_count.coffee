###
Module description
@class character_count
@main anunciante/publicacion
@author Ronny Cabrera
###

yOSON.AppCore.addModule "character_count", (Sb) ->
	defaults = {
		textArea : "textarea"
		actionCount : ".action_count"
	}
	st = {}
		
	dom = {}

	catchDom = () ->
		dom.textArea = $(st.textArea)
		dom.actionCount = $(st.actionCount)
		dom.textMaximun = 10
		return
	suscribeEvents = () ->
		dom.textArea.on "keyup focus", fn.countDown
		return
	events = {
		enevent : () ->
			#log("example")
			#return
	}
	fn = {
		countDown : () ->
			textCount = dom.textArea.val().length
			textLimit = dom.textMaximun - textCount
			if textCount > dom.textMaximun
				dom.textArea.val dom.textArea.val().substr(0, dom.textMaximun)
			if textLimit < 0
				textLimit = 0
			dom.actionCount.text textLimit
			return
			#log dom.textArea.val().length
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}
,[]