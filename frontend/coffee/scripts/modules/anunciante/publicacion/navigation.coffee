###
Module description
@class search_top
@main publication/natural_person
@author 
###

yOSON.AppCore.addModule "navigation", (Sb) ->
	defaults = {}
	st = {}
	dom = {}

	catchDom = () ->
		dom.parent = $(st.parent)
		dom.el     = $(st.el, dom.parent)
		return
	suscribeEvents = () ->
		dom.el.on "click",   events.camelCase
		return

	events = {
		camelCase : (e) ->
			log(e)
	}
	fn = {
		functionName : () ->
			log("example")
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}
,[]
