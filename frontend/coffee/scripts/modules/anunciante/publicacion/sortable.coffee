###
Module description
@class sortableDrop
@main anunciante/publicacion/daos-aviso
@author 
###

yOSON.AppCore.addModule "sortableDrop", (Sb) ->
	defaults =
		content          : ".box_uploader"
		sortable         : "#sortable"
		arraysOfSortable : "#sortable li"
		writeArrays      : ".arrays"	
	st = {}
	dom = {}

	catchDom = (st) ->
		dom.content          = $(st.content)
		dom.sortable         = $(st.sortable, dom.content)
		dom.arraysOfSortable = $(st.arraysOfSortable, dom.content)
		dom.writeArrays      = $(st.writeArrays, dom.content)
		return
	suscribeEvents = () ->
		dom.sortable.sortable()
		dom.sortable.disableSelection()
		dom.sortable.on 'sortupdate', fn.getChangeTo
		return
		
	fn =		
		getChangeTo: (event, ui) ->
			newArray = []
			catchDom(st)
			dom.arraysOfSortable.each (i, e) ->
				#myStr = $(e).find('.name').text()
				#myStrTwo = $.trim($(e).find('.name').text())
				newArray.push $(e).find('.name').text().trim()
				return
			log newArray
			dom.writeArrays.val(newArray)
			return
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		suscribeEvents()
		return

	return {
		init: initialize
	}
, []