###
Module description
@class get_suggested_price
@main anunciante/publicacion
@author Ronny Cabrera
###

yOSON.AppCore.addModule "get_suggested_price", (Sb) ->
	defaults = {
		parent             : ".publication"
		id_marca           : ".select_brand option:selected"
		id_modelo          : ".select_model option:selected"
		id_anio            : ".select_year option:selected"
		id_combustible     : ".select_fuel option:selected"
		id_transmision     : ".select_transmission option:selected"
		id_traccion        : ".select_traction option:selected"
		id_timon           : ".select_helm option:selected"
		chr_kilometraje    : ".input_mileage"
		chr_motor_cilindro : ".input_cylinder"
		url                : "/ajax/suggested-price/"
		formSeclects       : "form.publication select"
		formInputText      : "input.input_mileage, input.input_cylinder"
		suggestedPrice     : ".suggested_price"

	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.parent 				= $(st.parent)
		dom.id_marca          	= $(st.id_marca, dom.parent)
		dom.id_modelo           = $(st.id_modelo, dom.parent)
		dom.id_anio             = $(st.id_anio, dom.parent)
		dom.id_combustible      = $(st.id_combustible, dom.parent)
		dom.id_transmision      = $(st.id_transmisio, dom.parent)
		dom.id_traccion         = $(st.id_traccion, dom.parent)
		dom.id_timon            = $(st.id_timon, dom.parent)
		dom.chr_kilometraje     = $(st.chr_kilometraje, dom.parent)
		dom.chr_motor_cilindro  = $(st.chr_motor_cilindro, dom.parent)
		dom.formSeclects        = $(st.formSeclects)
		dom.formInputText  		= $(st.formInputText)
		dom.suggestedPrice      = $(st.suggestedPrice, dom.parent)
		return
	suscribeEvents = () ->
		dom.formSeclects.on "change", events.getPriceSelect
		dom.formInputText.on "blur", events.getPriceText
		return

	events = {
		getPriceSelect : (e) ->
			fn.getSuggestedPriceAjax()
		getPriceText : (e) ->
			fn.getSuggestedPriceAjax()
	}

	fn = {
		getSuggestedPriceAjax : () ->
			catchDom()
			$.ajax(
				url        : st.url
				method     : 'POST'
				data       : {
					id_marca            : dom.id_marca.val()
					id_modelo           : dom.id_modelo.val()
					id_anio             : dom.id_anio.val()
					id_combustible      : dom.id_combustible.val()
					id_tipo_transmicion : dom.id_transmision.val()
					id_traccion         : dom.id_traccion.val()
					id_tipo_timon       : dom.id_timon.val()
					chr_kilometraje     : dom.chr_kilometraje.val()
					chr_motor_cilindro  : dom.chr_motor_cilindro.val()
				}
				success    : fn.upDatePrice
				dataType   : 'Json')
			#log data
			log dom.id_marca.val()
			return
		upDatePrice : (data) ->
			#fn.finishLoading()
			#dom.suggestedPrice.html()
			dom.suggestedPrice.html(data.suggestedPrice)
			log 'te traigo el precio sugerido'
			return			
	}

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		log 'get_suggested_price'
		return

	return {
		init: initialize
	}