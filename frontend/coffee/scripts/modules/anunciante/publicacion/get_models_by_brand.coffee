###
Module description
@class get_models_by_brand
@main anunciante/publicacion
@author Ronny Cabrera
###

yOSON.AppCore.addModule "get_models_by_brand", (Sb) ->
	defaults = {
		parent : ".publication"
		selBrand : ".select_brand"
		selModel : ".select_model"
		url : "/ajax/common/modelos-by-marca/marca/"
		textMessage : "seleciona un modelo"
	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.parent = $(st.parent)
		dom.selBrand = $(st.selBrand, dom.parent)
		dom.selModel = $(st.selModel, dom.parent)
		return
	suscribeEvents = () ->
		dom.selBrand.on "change", events.selBrands
		return

	events = {
		selBrands : (e) ->
			ele   = $(e.target)
			brand = ele.val()
			progressAjax = $.ajax(
				url : st.url + brand
				method: 'POST'
				success: fn.updateModel
				beforeSend: fn.initLoading
				dataType: 'Json')
			#log brand 
			return
	}

	fn = {
		createItems : (value, name) ->
			return '<option value="' + value + '">' + name + '</option>'
		updateModel : (data) ->
			fn.finishLoading()
			dom.selModel.html('')
			#log data.length
			#optionMsg = '<option value="">' + st.textMessage + '</option>'
			i = 0
			while i < data.length
				dom.selModel.append fn.createItems(data[i].key, data[i].value)
				#log dom.selModel.append fn.createItems(data[i].key, data[i].value)
				#dom.selModel.html(optionMsg)
				i++
			dom.selModel.removeAttr('disabled')
			return
		initLoading : () ->			
			log "estoy cargando"
			return						
		finishLoading : () ->
			log "ya cargue"
			return
	}

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		log 'get_models_by_brand'
		return

	return {
		init: initialize
	}