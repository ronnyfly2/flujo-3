###
collapser
@class index
@main modules/ficha-aviso
@author Juan Pablo
###

yOSON.AppCore.addModule "collapser", (Sb) ->
	defaults = {
		parent      : ".collapser"
		el 			: ".box_expandible"
	}
	st = {}
	dom = {}
	catchDom = (st) ->
		dom.parent = $(st.parent)
		dom.el = $(st.el, dom.parent)
		return
	suscribeEvents = () ->
		dom.el.on 'click', fn.bindCollapse
		return
	fn = {
		bindCollapse: (e) ->
			e.preventDefault()
			self = $(@)
			if self.hasClass('open')
				self.removeClass('open')
			else
				self.closest(st.parent).find(st.el).removeClass('open')
				self.addClass('open')
			return
		run : (e)->
			# test
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		suscribeEvents()
		return
	return {
		init: initialize
	}
,[]