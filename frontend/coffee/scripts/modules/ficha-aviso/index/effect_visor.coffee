###
Module realiza el effecto al visor de imagenes para ficha
@class effect_visor
@main ficha-aviso/index
@author Jhonnatan Castro
###

yOSON.AppCore.addModule "effect_visor", (Sb) ->
	defaults = {
		mask 	: ".mask_gallery > a"
		images 	: ".mask_gallery > a img"
		effect 	: "fade"
		loading : ".loading"
	}
	st = {}
	dom = {}
	catchDom = () ->
		dom.mask   = $(st.mask)
		dom.images = $(st.images)
		dom.loading = $(st.loading)
		return
	fn = {
		
		mainEffect : (effect)->
			switch effect
				when "fade" then fn.doEffectFade()
			return

		doEffectFade : ->
			dom.mask.find(".change").addClass("opaque")
			return

		addMask : (images) ->
			dom.loading.hide()
			$(st.images).remove()			
			#imgCurrent 	= $("<img>", src : images.current)
			imgChange 	= $("<img>", src : images.change, class : "change")
			dom.mask.append(imgChange)
			return

		showImage : (images)->
			fn.addMask(images)			
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		Sb.events(['effect_visor:show_image'], fn.showImage, this)        
		catchDom()		
		return

	return {
		init: initialize
	}