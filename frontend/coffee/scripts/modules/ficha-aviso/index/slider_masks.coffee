###
Module que genera el slider para los masks de galeria para mobile
@class slider_masks
@main ficha-aviso/index
@author Jhonnatan Castro
###

yOSON.AppCore.addModule "slider_masks", (Sb) ->
	defaults = {
		wrapper 	: ".list_masks"
		gallery 	: ".gallery"
		prev 		: ".icon_arrow_left.mask"
		next 		: ".icon_arrow_right.mask"
		slickDots 	: ".slick-dots"	
	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.wrapper = $(st.wrapper)
		dom.gallery = $(st.gallery)
		return

	afterCatchDom = ->
		Sb.trigger("carrusel_images:loadSlick", {
			element: dom.wrapper
			config : 					
				slidesToShow 	: 1
				slidesToScroll	: 1
				infinite 		: false
				prevArrow 		: dom.gallery.find(st.prev)
				nextArrow 		: dom.gallery.find(st.next)
			
		},
		(slider)->
			log "slider mask", slider
			return
		)		
		return
	
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		afterCatchDom()
		#suscribeEvents()
		return

	return {
		init: initialize
	}