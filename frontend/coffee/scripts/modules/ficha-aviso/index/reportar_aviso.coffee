###
Funcionalidad reportar aviso
@class index
@main modules/reportar_aviso
@author Juan Pablo
###

yOSON.AppCore.addModule "reportar_aviso", (Sb) ->
	defaults = {
		parent      : ".modal_report"
		sendBtn		: "button"
		tpl			: "#reportarAviso"
	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.parent = $(st.parent)
		dom.sendBtn = $(st.sendBtn, dom.parent)
		return
	suscribeEvents = () ->
		dom.sendBtn.on 'click', fn.sendBtn
		return
	fn = {
		sendBtn : (e)->
			e.preventDefault()
			alert('click en boton, ahora cerramos modal')

			# lanzar close cuando todo este correcto
			Sb.trigger('fancybox:close')
			log e
			return
		syncDom: () ->
			catchDom()
			suscribeEvents()
			log "syncDom reportarAviso"
			return
		beforeModal: (id) ->
			log "before modal"
			log id
			data = {view: 2, nroAlertas: 0, nroToken: "3adb752f2325f877a108fa27da67095d"}
			
			# simulando ajax
			setTimeout (->
				Sb.trigger('fancybox:open', {})
				return
			), 500
			return
		register: () ->
			return {identificator:st.tpl, after:fn.syncDom, before:fn.beforeModal}
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		Sb.trigger 'fancybox:register', fn.register
		# Sb.trigger 'fancybox:after', fn.syncDom
		# Sb.trigger 'fancybox:before', fn.beforeModal
		return
	return {
		init: initialize
	}
,[]