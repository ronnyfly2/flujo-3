###
Module que muestra la imagen del thumbnail en la vista preliminar
@class visor_thumbnail
@main ficha-aviso/index
@author 
###

yOSON.AppCore.addModule "visor_thumbnail", (Sb) ->
	defaults = {
		parent      : ".thumbs_gallery"
		slides		: ".item"
		el          : "div a"
		mask 		: ".mask_gallery a img"
		loading 	: ".loading"
	}
	st = {}
	dom = {}
	images = {}

	catchDom = () ->
		dom.parent = $(st.parent)
		dom.slides = $(st.slides, st.parent)
		dom.el     = $(st.el, dom.parent)
		dom.mask   = $(st.mask)
		dom.loading = $(st.loading)
		return

	afterCatchDom = ->
		images.current = dom.mask.attr("src")
		return

	suscribeEvents = () ->
		dom.el.on "click",   events.clickThumb
		return

	events = {
		clickThumb : (e) ->
			e.preventDefault()
			thumb = $(e.target).parents("a")			
			images.change = thumb.data("view")
			fn.changeItemActive(thumb)
			fn.preloadImages()
			dom.loading.show()
			return
	}
	fn = {
		changeItemActive : (thumb)->			
			item = thumb.parents(".item")
			dom.slides.removeClass("active")
			item.addClass("active")
			return

		preloadImages : ->
			listImages = [images.current, images.change]			
			beforeLoadImages
				images : listImages
				MaxNumberImagesLoad : listImages.length
				callback : ->
					Sb.trigger("effect_visor:show_image", images)
					return
			return		
	}

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		afterCatchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}