###
Features Nuevo
@class features_nuevo
@main neoauto/ficha-aviso/features_nuevo
@author Elizabeth Manrique
###
yOSON.AppCore.addModule "features_nuevo", (Sb) ->
    defaults = {
        parent                 : ".features_nuevo",
        content                 : ".features_nuevo_content",
        maskContent            : ".u_table_values_content",
        flexContent            : ".u_flex_content",
        tableFeature           : ".feature_value_table",
        moreVersions           : ".features_more_versions",
        indexLastViewedVersion : 0
    }
    st = {}
    dom = {}
    MAX_WIDTH_BREAKPOINT_MOBILE = 668
    MAX_WIDTH_BREAKPOINT_TABLET = 1000
    timeOutMoreVersions = null
    responsiveWindow = null

    catchDom = () ->        
        dom.parent       =  $(st.parent)
        dom.content      =  $(st.content, st.parent)
        dom.maskContent  =  $(st.maskContent, st.parent)
        dom.flexContent  =  $(st.flexContent, st.maskContent )
        dom.tableFeature =  $(st.tableFeature, st.flexContent)
        dom.moreVersions =  $(st.moreVersions, st.parent)     
        dom.window       =  $(window)   

        return
    afterCatchDom = () ->
        # init validation to features nuevo
        fn.prepareFlexContent(fn.setResponsiveWidthToTables)        
        return
    suscribeEvents = () ->
        dom.moreVersions.on('click', events.getNextVersion)
        dom.window.on('resize', events.setResponsiveWidthToTables)
        return        
    events = {
        getNextVersion: (e) ->
            fn.moveFlexContent()
            return
     
        setResponsiveWidthToTables: () ->            
            fn.setResponsiveWidthToTables()
            return
    }

    fn = {
        prepareFlexContent:(callback) ->
            maxWidth = dom.tableFeature.css('max-width')
            dom.tableFeature.attr('data-original-max-width', maxWidth);            
            callback();
            return
        setWidthToTablesForMobile: ->            
            totalVersion = dom.parent.attr('data-total-version') || 3
            currentWidthMask = dom.maskContent.width()
            configCssToFlex  = {}
            configCssToTable = {}

            configCssToFlex = {
                'width' : parseInt(currentWidthMask * totalVersion)
            }

            configCssToTable = {
                'width' : currentWidthMask,
                'max-width' : currentWidthMask
            }

            dom.tableFeature.each((item) ->                
                $(dom.tableFeature[item]).css(configCssToTable)
            )

            dom.flexContent.each((item) ->  

                $(dom.flexContent[item]).css(configCssToFlex)
            )

            return

        setWidthToTablesForTablet: ->            
            totalVersion = dom.parent.attr('data-total-version') || 3
            currentWidthMask = dom.maskContent.width()
            configCssToFlex  = {}
            configCssToTable = {}

            configCssToFlex = {
                'width' : parseInt(currentWidthMask / 2 * totalVersion)
            }

            configCssToTable = {
                'width' : currentWidthMask/2,
                'max-width' : currentWidthMask/2
            }

            dom.tableFeature.each((item) ->                
                $(dom.tableFeature[item]).css(configCssToTable)
            )

            dom.flexContent.each((item) ->                 
                $(dom.flexContent[item]).css(configCssToFlex)
            )

            return

        setWidthToTablesForDesktop: ->
            totalVersion = dom.parent.attr('data-total-version') || 3
            maxWidthDefaultTable = dom.tableFeature.attr('data-original-max-width')
            configCssToFlex = {}
            configCssToTable = {}

            switch totalVersion

                when "1"
                    configCssToFlex = {
                        'width' : '100%'
                    }
                    configCssToTable = {
                       'width'     : '100%',
                       'max-width' : '100%'
                    }

                    
                when "2"
                    configCssToFlex = {
                        'width' : '100%'
                    }

                    configCssToTable = {
                        'width'     : '50%',
                        'max-width' : '50%'
                    }
                    
                else
                    configCssToFlex = {
                        'width' : parseInt(maxWidthDefaultTable.replace('px', '') * totalVersion)
                    }

                    configCssToTable = {
                        'width' : maxWidthDefaultTable,
                        'max-width' : maxWidthDefaultTable
                    }

            dom.tableFeature.each((item) ->                
                $(dom.tableFeature[item]).css(configCssToTable)
            )

            dom.flexContent.each((item) -> 
                $(dom.flexContent[item]).css(configCssToFlex)                
            )

            return
        updateLoading: (isShow) ->
            
            if isShow
                dom.parent.addClass('is_loading')

            else 
                dom.parent.removeClass('is_loading')

        setResponsiveWidthToTables: () ->   
            fn.updateLoading(true)
            clearTimeout(responsiveWindow)
            responsiveWindow = setTimeout(() ->
                if fn.isMobile()
                    fn.setWidthToTablesForMobile()
                else 
                    if fn.isTablet()
                        fn.setWidthToTablesForTablet()
                    else 
                        fn.setWidthToTablesForDesktop()

                fn.evaluateMoreVersions(fn.updateLoading, false)              
            , 500)

            return

        evaluateMoreVersions: (callback) ->
            clearTimeout(timeOutMoreVersions)
            timeOutMoreVersions = setTimeout(() ->                               
                fn.updateCurrentIndexLastViewedVersion()
            , 1000)
            callback(arguments[1])
            return

        isDesktop: () -> 
            currentWidth = dom.window.width()

            if currentWidth > MAX_WIDTH_BREAKPOINT_TABLET
                return true
            return 

        isTablet: () -> 
            currentWidth = dom.window.width()

            if currentWidth > MAX_WIDTH_BREAKPOINT_MOBILE  && currentWidth <= MAX_WIDTH_BREAKPOINT_TABLET
                return true
            return false

        isMobile: () ->
            currentWidth = dom.window.width()

            if currentWidth < MAX_WIDTH_BREAKPOINT_MOBILE
                return true
            return false

        updateStateMoreVersions: () ->       
            if fn.isMoreVersionsAvailable()
                dom.moreVersions.show()
            else 
                dom.moreVersions.hide()

            return

        getIndexLastViewedVersion: () ->
            currentLeft     =  parseInt(dom.flexContent.css('left').replace('px', ''))
            pixelsToMove    =  dom.tableFeature.width()
            totalVersion    =  dom.parent.attr('data-total-version') || 3
            maxPixelsToMove =  -(pixelsToMove * (totalVersion - dom.maskContent.width() / dom.tableFeature.width()))

            if maxPixelsToMove == currentLeft
                indexLastViewedVersion = totalVersion - 1
            else
                indexLastViewedVersion = Math.abs((maxPixelsToMove - currentLeft) / pixelsToMove)

            indexLastViewedVersion = Math.floor(indexLastViewedVersion)
            
            return  indexLastViewedVersion

        updateCurrentIndexLastViewedVersion: () ->            
            ###storedWidth  = 0
            tables       = dom.flexContent.first().find(".feature_value_table")
            pixelsToMove = dom.tableFeature.width()
            newLeft      = 0

            for table in tables                
                storedWidth = storedWidth + $(table).width()

                if storedWidth == dom.maskContent.width()
                   break;

            if st.indexLastViewedVersion != 0
                newLeft = (st.indexLastViewedVersion * pixelsToMove) - storedWidth
            
            ###
            fn.moveFlexContent(0) 
            return

        moveFlexContent: (newLeft) ->
            currentLeft  = parseInt(dom.flexContent.css('left').replace('px', ''))
            pixelsToMove = dom.tableFeature.width()
            newLeft      = if newLeft == 0  then 0 else currentLeft - pixelsToMove            
            
            dom.flexContent.css('left': newLeft)
            fn.updateStateMoreVersions()
            st.indexLastViewedVersion = fn.getIndexLastViewedVersion()

            return

        isMoreVersionsAvailable: (pixelsToMove) ->
            currentLeft     =  parseInt(dom.flexContent.css('left').replace('px', '')) 
            pixelsToMove    =  pixelsToMove || dom.tableFeature.width()
            totalVersion    =  dom.parent.attr('data-total-version') || 3     
            maxPixelsToMove =  -(pixelsToMove * (totalVersion - Math.floor(dom.maskContent.width()/dom.tableFeature.width())))

            if maxPixelsToMove == currentLeft || maxPixelsToMove == 0
                return false
            
            return true

    }
    initialize = (opts) ->
        st = $.extend({}, defaults, opts)
        catchDom()
        afterCatchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }
,[]