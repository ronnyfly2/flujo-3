###
funcionalidad de visor de imagenes
@class slider_thumbnail
@main ficha-aviso/index
@author 
###

yOSON.AppCore.addModule "slider_thumbnail", (Sb) ->
	defaults = {
		parent 		: ".gallery"
		galleryThumbs: ".thumbs_gallery"
		wrapper 	: ".wrapper"
		prev 		: ".icon_arrow_left.thumb"
		next 		: ".icon_arrow_right.thumb"
		slickDots 	: ".slick-dots"	
	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.parent 		= $(st.parent)		
		dom.wrapper 	= $(st.wrapper, dom.parent)
		dom.galleryThumbs = $(st.galleryThumbs)
		return

	afterCatchDom = ->
		fn.initSlider()
		return
	
	fn = {
		initSlider : ->
			Sb.trigger("carrusel_images:loadSlick", {
				element: dom.wrapper
				config : 					
					slidesToShow 	: 4
					slidesToScroll	: 1
					infinite 		: false
					prevArrow 		: dom.galleryThumbs.find(st.prev)
					nextArrow 		: dom.galleryThumbs.find(st.next)
				
			},
			(slider)->
				log "slider thumbnail", slider
				return
			)					
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		afterCatchDom()		
		return

	return {
		init: initialize
	}