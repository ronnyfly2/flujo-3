###
Funcionalidad enviar amigo
@class index
@main modules/enviar_amigo
@author Juan Pablo
###

yOSON.AppCore.addModule "enviar_amigo", (Sb) ->
	defaults = {
		parent      : ".modal_send_friend"
		sendBtn		: "button"
		form 		: "form"
		tpl 		: "#enviarAmigo"
		tplLogin 	: "#tplLogin"
		logged 		: false
		message 	: ".message"
		urlAjax		:
			getData	: "/ajax/ficha/get-data-send-friend"
			sendData: "/ajax/ficha/send-friend"
	}
	dom = {}
	st = {}
	catchDom = () ->
		dom.parent = $(st.parent)
		dom.sendBtn = $(st.sendBtn, dom.parent)
		dom.form = $(st.form, dom.parent)
		dom.message = $(st.message, dom.parent)
		return
	suscribeEvents = () ->
		dom.sendBtn.on 'click', fn.sendBtn
		dom.form.parsley()
		dom.form.on("submit", events.submitForm)
		return
	events = {
		submitForm : (e)->
			e.preventDefault()
			# Sb.trigger('fancybox:close')
			Sb.trigger('fancybox:showLoading')
			$.ajax
				type    : "POST"
				url     : st.urlAjax.sendData
				data 	: $(@).serialize()
				dataType: "json"
				success : (json) ->
					Sb.trigger('fancybox:hideLoading')
					# error del sistema (status=>false, data=>null)
					if(!json.status && !json.data)
						dom.message.text(json.message)
					# error de validation (status=>false, data=>object)
					else if(!json.status && json.data)
						dom.message.text(json.message)
					# correcto json.status=>true
					else
						Sb.trigger('fancybox:open',json,'#messageModal')

					return
			log e
			return
	}

	fn = {
		afterModal: () ->
			if st.logged
				catchDom()
				suscribeEvents()
				Sb.trigger('recaptcha:renderReCaptcha', dom.parent.find('.g-recaptcha'))
			else
				Sb.trigger('login:asyncDom')
			return
		beforeModal: (obj) ->
			$.ajax
				type    : "POST"
				url     : st.urlAjax.getData
				data 	: {urlId:obj['data-idaviso']}
				dataType: "json"
				success : fn.loadAjaxGetData
			return
		loadAjaxGetData: (json) ->
			log json
			if json.status
				st.logged = json.data.logged
				if(st.logged)
					Sb.trigger('fancybox:open', json.data)
				else
					Sb.trigger('fancybox:open', json.data, '#tplLogin')
			else
				log "error inesperado al cargar ajax enviarAmigo"
				Sb.trigger('fancybox:hideLoading')
			return
		register: () ->
			return {identificator:st.tpl, after:fn.afterModal, before:fn.beforeModal}
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		Sb.trigger 'fancybox:register', fn.register
		return
	return {
		init: initialize
	}
,[
	"js/dist/libs/parsleyjs/dist/parsley.min.js"
    "js/dist/libs/parsleyjs/src/i18n/es.js"
]