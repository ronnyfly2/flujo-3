###
Registro
@class contact_advertiser
@main neoauto/auth/contact_advertiser
@author Elizabeth Manrique
###
yOSON.AppCore.addModule "contact_advertiser", (Sb) ->
    defaults = {
        parent          : ".contact_advertiser",
        formSendMessage : ".form_send_message",
        formViewPhone   : ".form_view_phone",
    }
    st = {}
    dom = {}

    catchDom = () ->
        dom.parent              = $(st.parent)
        dom.formSendMessage      = $(st.formSendMessage,st.parent)
        dom.formViewPhone        = $(st.formViewPhone, dom.parentPassword)
        return
    afterCatchDom = () ->
        # init validation to form register        
        fn.setParsleyByForm()
        
        return
    suscribeEvents = () ->
        dom.formSendMessage.on('submit',events.sendFormByAjax)        
        dom.formViewPhone.on('submit', events.sendFormByAjax)
        return
    events = {    
        sendFormByAjax : (e) -> 
            e.preventDefault()
            self = $(@)
            self.toggleClass('is_loading')

            if self.parsley().isValid()
                url = self.attr('action');
                payload = self.serialize();

                $.post(
                    url,
                    payload
                ). done((data) -> 
                    status  = data.status
                    errors  = data.data
                    message = data.message

                    fn.updateResultMessage(self, data)

                    if status
                        fn.clearForm(self)
                    else                    
                        if errors
                            fn.parseErrors(self, errors)
                            recaptcha = self.find('.g-recaptcha')
                            Sb.trigger('recaptcha:resetReCaptcha', recaptcha)
                    
                    self.toggleClass('is_loading')                        
                )

            
    }
    fn = {
        parseErrors: (form, errors) ->
            
            for key of errors
                value = errors[key]
                el = form.find('input[name="'+ key + '"]')                
                errorList = el.parent().find('.parsley-errors-list')                
                html = ''

                for key_error of value
                    html = html + '<li class="parsley-"'+ key_error + '">' + value[key_error] + '</li>'

                errorList.html(html)

            return
        updateResultMessage: (form, data) ->            
            resultMessage = form.parent().find('.resultMessage')
            message = data.message
            status  = data.status
            classResult = ''
            
            resultMessage.show()
            resultMessage.find('p').html(message)

            if !status
                resultMessage.addClass('error')

            else
                resultMessage.removeClass('error')

            return

        clearForm: (form) ->            
            form.find('input, textarea').each((index, item) ->                 
                $(item).val('')
                $(item).next('.parsley-errors-list').html('')
            )


        setParsleyByForm : () ->
            dom.formSendMessage.each((item) -> 
                $(dom.formSendMessage[item]).parsley()
            )
            
            dom.formViewPhone.each((item) -> 
                $(dom.formViewPhone[item]).parsley()
            )

            return
        setParsleyLanguage : (lang) ->
            window.ParsleyValidator.setLocale(lang)
            return
    }
    initialize = (opts) ->
        st = $.extend({}, defaults, opts)
        catchDom()
        afterCatchDom()
        suscribeEvents()
        fn.setParsleyLanguage('es')   
        return

    return {
        init: initialize
    }
,[
    'js/dist/libs/parsleyjs/src/i18n/es.js',
    "js/dist/libs/parsleyjs/dist/parsley.min.js"
]