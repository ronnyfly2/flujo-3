###
Module que genera el zoom del imagen seleccionada
@class zoom_visor
@main ficha-aviso/index
@author Jhonnatan Castro
###

yOSON.AppCore.addModule "zoom_visor", (Sb) ->
	defaults = {
		parent		: ".thumbs_gallery"
		slides		: ".item"
		optionsView : ".options_view"
		btnZoom 	: ".zoom"
		gallery 	: ".gallery"
		maskGallery : ".mask_gallery a"
	}
	st = {}
	dom = {}

	itemSelected = 0

	catchDom = () ->
		dom.parent 		= $(st.parent)
		dom.slides     	= $(st.slides, dom.parent)
		dom.btnZoom 	= $(st.btnZoom, st.optionsView)
		dom.maskGallery = $(st.maskGallery, st.gallery)
		return

	suscribeEvents = () ->
		dom.maskGallery.on "click", events.viewZoom
		dom.btnZoom.on "click", events.viewZoom
		return

	events = {
		viewZoom : (e) ->
			e.preventDefault()
			fn.showFancybox()
			return
	}
	fn = {
		getImagesFb: ->
			images = []
			totalImages = dom.slides.length || 0
			pos = 0
			while pos < totalImages
				data = fn.getImageByPosition(pos)
				images.push(data)
				pos++
			images		

		getImageByPosition : (pos)->
			data = {}

			#ele = if dom.slides.length > 0 then dom.slides else dom.mask_gale
			ele = dom.slides

			item = ele.eq(pos)

			data.middle = item.find("a").data("view")
			data.type = "image"
			data.href = item.find("a").data("modal")

			data

		getImageSelected : ->
			#selected = itemSelected
			#log "dom.slides.filter('.active').index()", dom.slides.filter('.active').index()
			#if (dom.slides.filter('.active').index()>0)
			itemSelected = dom.slides.filter('.active').index()
			
			itemSelected

		showFancybox : ->
			images = fn.getImagesFb()
			$.fancybox.open images,
				openEffect      : 'fade'
				closeEffect 	: 'fade'
				prevEffect      : 'fade'
				nextEffect      : 'fade'
				index           : fn.getImageSelected()
				padding         : 8
				tpl :
					wrap    : "<div class=\"fancybox-wrap\" tabIndex=\"-1\"><div class=\"fancybox-skin ficha\"><div class=\"fancybox-outer\"><div class=\"fancybox-inner\"></div></div></div></div>"
					image   : "<img class=\"fancybox-image\" src=\"{href}\" alt=\"\" />"
					iframe  : "<iframe id=\"fancybox-frame{rnd}\" name=\"fancybox-frame{rnd}\" class=\"fancybox-iframe\" frameborder=\"0\" vspace=\"0\" hspace=\"0\" allowtransparency=\"true\"></iframe>"
					error   : "<p class=\"fancybox-error\">El contenido solicitado no se puede cargar.<br/>Por favor, inténtelo de nuevo más tarde.</p>"
					closeBtn: "<a title=\"Close\" class=\"fancybox-item fancybox-close\" href=\"javascript:;\"></a>"
					next    : "<a title=\"Siguiente\" class=\"icon fancybox-nav fancybox-next\" href=\"javascript:;\"><span></span></a>"
					prev    : "<a title=\"Anterior\" class=\"icon fancybox-nav fancybox-prev\" href=\"javascript:;\"><span></span></a>"
				helpers:
					title   :
						type: "inside"
				afterLoad: ->
					@title = "Foto " + (@index + 1) + " de " + @group.length
					return
			return
	}
   
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}
,[	"js/dist/libs/fancybox/source/jquery.fancybox.js"
	'js/dist/libs/fancybox/source/jquery.fancybox.pack.js'
	'js/dist/libs/fancybox/source/helpers/jquery.fancybox-buttons.js'
	'js/dist/libs/fancybox/source/helpers/jquery.fancybox-media.js'
]