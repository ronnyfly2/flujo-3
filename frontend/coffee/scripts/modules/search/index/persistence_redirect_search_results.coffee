###
Modulo que cancela el evento redirect para los elementos con la clase no_clic.
@class persistence_redirect_search_results
@main search/index
@author Jhonnatan Castro
###

yOSON.AppCore.addModule "persistence_redirect_search_results", (Sb) ->
    defaults = {
        parent      : ".item_car > a"
        noClic      : "no_clic"
    }
    st = {}

    dom = {}

    catchDom = () ->
        dom.parent = $(st.parent)
        #dom.noClic = $(st.noClic)
        return
    suscribeEvents = () ->
        dom.parent.on "click", events.clickItemCar
        return

    events = {
        clickItemCar : (e) ->
            ele = $(e.target)
            window.myele = ele         
            if ele.hasClass(st.noClic)
                #log "entro"
                href = ele.data("href")
                #log "href", ele.data("href")
                if href != undefined
                    console.log("href", href)
                    e.preventDefault()
                    window.location = href
                else
                    return false
            return true
    }
    fn = {
        functionName : () ->
            console.log("example")
            return
    }
    initialize = (opts) ->
        st = $.extend({}, defaults, opts)
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }