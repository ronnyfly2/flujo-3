###
init_ellipsis
@class modal_login
@main modules/search
@author Ronny Cabrera
###

yOSON.AppCore.addModule "init_ellipsis", (Sb) ->
	defaults = {
		parent      : ".item_body"
		description : ".itm_description"
	}
	st = {}
	dom = {}
	catchDom = (st) ->
		dom.parent = $(st.parent)
		dom.description = $(st.description, dom.parent)
		return
	suscribeEvents = () ->
		fn.runDotdotdot()
		return
	fn = {
		callbackDotdotdot : ()->
			dom.description.css visibility: "visible"
			return
		runDotdotdot : (e)->
			catchDom(st)
			dom.description.dotdotdot callback: fn.callbackDotdotdot
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		suscribeEvents()
		return
	return {
		init: initialize
	}
,['js/dist/libs/jQuery.dotdotdot/src/js/jquery.dotdotdot.js']