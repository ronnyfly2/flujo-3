###
Filtro search_filters
@class search_filters
@main modules/all
@author Juan Pablo Chullo
###

yOSON.AppCore.addModule "search_filters", (Sb) ->
	defaults = {
		parent       		: ".search_filters"
		boxExpand    		: ".box_expand"
		boxContent 	 		: ".box_content"
		linkContent  		: ".more"
		linkExpand 	 		: ".see_more"
		linkFilter 	 		: ".icon_filter"
		main_search   		: ".main_search"
		bodyResults  		: ".body_results"
		filterMobile 		: ".option_filters_mobile"
		showItems 	 		: ".showing_items"
		changeDepartament 	: ".changeDepartament"
		changeDistrict 		: ".changeDistrict"
		loaderCombo 		: ".loader"
		filtersInput 		: ".filters"
		filterForm 			: ".filter_form"
		inputForm 			: ".input_form"
		linkDisabled 		: ".disabled"
		# extra
		backTop 			: ".back_top"
	}
	st = {}
	dom = {}
	text_filter_toggle = "Menos opciones"
	url_ajax_district = "/ajax/search/distritos"

	catchDom = () ->
		dom.document	 		= $(document)
		dom.parent 				= $(st.parent)
		dom.boxExpand  	 		= $(st.boxExpand, dom.parent)
		dom.linkExpand   		= $(st.linkExpand, dom.parent)
		dom.linkFilter 	 		= $(st.linkFilter)
		dom.bodyResults  		= $(st.bodyResults)
		dom.filterMobile 		= $(st.filterMobile)
		dom.showItems 	 		= $(st.showItems)
		dom.main_search   		= $(st.main_search)
		dom.changeDepartament 	= $(st.changeDepartament)
		dom.changeDistrict 		= $(st.changeDistrict)
		dom.loaderCombo 		= $(st.loaderCombo, dom.parent)
		dom.filtersInput 		= $(st.filtersInput, dom.parent)
		dom.filterForm 			= $(st.filterForm, dom.parent)
		dom.inputForm 			= $(st.inputForm, dom.parent)
		dom.linkDisabled 		= $(st.linkDisabled, dom.parent)
		dom.backTop 			= $(st.backTop)
		return
	afterCatchDom = () ->
		$(".inner_showing_items").html(dom.showItems.clone())
		return
	suscribeEvents = () ->
	    dom.boxExpand.on("click", events.boxToggle)
	    dom.linkExpand.on("click", events.linksToggle)
	    dom.linkFilter.on("click", events.filterToggle)
	   	dom.document.on('touchmove scroll', events.scroll)
	   	dom.changeDepartament.on('change', events.changeDepartament)
	   	dom.filterForm.on('submit', events.filterForm)
	   	dom.linkDisabled.on('click', events.linkDisabled)
	   	dom.backTop.on('click', events.backTop)
	   	# test
	   	dom.bodyResults.swipe (direction, offset) ->
	   		# log direction
	   		# log offset
	   		if(dom.main_search.hasClass('open'))
	   			if(direction.x=='left')
	   				dom.linkFilter.trigger('click')
	   		return
	    return

	events = {
		scroll: (e) ->
			scrolltop = $(@).scrollTop()
			if(scrolltop>105)
				dom.filterMobile.addClass('sticky')
			else
				dom.filterMobile.removeClass('sticky')
			return

		boxToggle: (e) ->
			ele = $(@)
			fn.toggleListCookie(ele.data('expand'))
			ele.parent().toggleClass('collapse')			
			return
		
		linksToggle: (e) ->
			e.preventDefault();
			fn.textToggle($(@))
			box = $(@).closest(st.boxContent)
			box.find(st.linkContent).slideToggle("fast")
			# extra			
			$("html,body").animate({scrollTop:box.offset().top-30}, 500)
			return
		
		filterToggle: (e) ->
			ele = $(e.target)
			$(st.filterToggle).removeClass('active')
			if(ele.hasClass('active'))
				ele.removeClass('active')
				dom.main_search.removeClass('open')
			else
				ele.addClass('active')
				dom.main_search.addClass('open')
			return

		changeDepartament: (e) ->
			departament = $(e.target).val()
			filters = dom.filtersInput.serialize()
			params = "idUbicacionDepartamento="+departament+"&"+filters
			fn.ajaxLoadDistrict(params)
			return

		filterForm: (e) ->
			self = $(e.target)
			$.each dom.filtersInput.serializeArray(), (i,obj) ->
				$("<input>").attr({
					'type':'hidden',
					'name': obj.name
				}).val(obj.value).prependTo(self)
				return
			return
		linkDisabled: (e) ->
			e.preventDefault()
			# stop
			return
		backTop: (e) ->
			e.preventDefault()
			$("html,body").animate({scrollTop:0}, 1000)
			return
		
	}
	fn = {
		textToggle: (el) ->
			text = el.text()
			el.text( text_filter_toggle )
			text_filter_toggle = text
			return
		toggleListCookie: (str) ->
			nameCookie = "filter_box_collapse"
			cookie = Cookie.read(nameCookie) || '[]'
			jparse = window.JSON.parse(cookie)
			if(jparse.length>0)
				arrayDelete = $.inArray(str, jparse)
				log arrayDelete
				if(arrayDelete != -1)
					jparse.splice(arrayDelete, 1)
				else
					jparse.push(str)
				Cookie.create(nameCookie, window.JSON.stringify(jparse))
			else
				jparse = [str]
				Cookie.create(nameCookie, window.JSON.stringify(jparse))
			return
		ajaxLoadDistrict: (params) ->
			window.neo_ajax
					url : 	url_ajax_district
					type:	"json",
					method: "post",
					data: params
				,
				dom.loaderCombo,
				fn.ajaxCallbackDistrict
			return
		ajaxCallbackDistrict: (result) ->
			t = []
			if(result.status)
				$.each result.data.districts, (i,el)->
					t.push("<option value='"+el.slug+"'>"+el.nombre+"</option>")
					return
			dom.changeDistrict.html(t.join(''))
			log t
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		afterCatchDom()
		suscribeEvents()
		log "search_filters"
		return

	return {
		init: initialize
	}
,[]
