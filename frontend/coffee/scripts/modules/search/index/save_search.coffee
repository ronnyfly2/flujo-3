###
Module popup de salvar busqueda
@class save_search
@main modules/search
@author Ronny Cabrera
###

yOSON.AppCore.addModule "save_search", (Sb) ->
	defaults = {
		
		parent    : ".search_filters"                
		linkSave  : ".save_filter a"		
		
		box       : ".popup_save"
		close     : ".close"
		form 	  : ".frm_save_search"
		switcher  : ".switchery"
		moreOptions : ".list_responses" 
		
		tplSaveSearch : "#tplSaveSearch"
		tplSaveSearchMessage : "#tplSaveSearchMessage"

		ajax 	:
			verificarAlertas: "/default/buscarautos/ajax-verificar-alertas"
			saveAlertas 	: "/default/buscarautos/ajax-guardar-alerta/"

		btnCancel : ".btn.bg_gray"
	}
	st = {}
	dom = {}
	icons = {}
	touchMove = true

	afterCatchDom = () ->        
		fn.configUnderscore()
		return

	catchDom = (st) ->
		dom.document = $(document)
		dom.parent = $(st.parent)
		dom.linkSave = $(st.linkSave, dom.parent)		
		
		dom.box = $(st.box)
		dom.close = $(st.close, dom.box)
		dom.tplSaveSearch = $(st.tplSaveSearch)
		dom.tplSaveSearchMessage = $(st.tplSaveSearchMessage)

		dom.moreOptions = $(st.moreOptions, dom.box)
		return

	asyncCatchDom = ->
		dom.switcher = document.querySelector(st.switcher)
		dom.btnCancel = $(st.btnCancel, dom.box)
		dom.form = $(st.form)		
		return

	suscribeEvents = () ->        
		dom.linkSave.on "click", events.getAjaxLogged
		dom.parent.on "click", st.close, events.hidePopUp
		dom.document.on "touchmove", events.touchmove
		return

	asyncSuscribeEvents = ->
		dom.switcher.onchange = events.switcher
		dom.btnCancel.on("click", events.hidePopUp)
		dom.form.on("submit", events.saveSearch)
		dom.form.parsley()
		return

	events = {
		touchmove : (e)->
			if touchMove is false
				e.preventDefault()
			return

		setNoTouchMove : ->
			touchMove = false
			return

		setTouchMove : ->
			touchMove = true
			return

		switcher : ->
			if this.checked is true				
				dom.moreOptions.show()
			else
				dom.moreOptions.hide()			
			return

		getAjaxLogged : (e) ->
			$.ajax
				url      : st.ajax.verificarAlertas
				beforeSend : $.fancybox.showLoading
				type     : "POST"
				dataType : "json"
				success  : events.returnView			
			return
		
		returnView : (data)->
			$.fancybox.hideLoading()
			param = parseInt(data.view)			
			switch param
				when 1 then events.viewSaveSearch(data)
				when 2 then events.showModal(data)
				when 3 then events.viewErrorSearch(data)
			return

		saveSearch : (e)->
			#$.fancybox.close()
			form = $(e.target)
			if (form.parsley().isValid())
				$.ajax
					data    : dom.form.serialize()
					beforeSend : $.fancybox.showLoading
					type  	: 'POST'
					url   	: st.ajax.saveAlertas
					success : events.returnSubmitSaveSearch
				e.preventDefault()
			return

		returnSubmitSaveSearch : (data)->
			data["tpl"] = if data.success is false then "error" else "ok"
							
			template = _.template(dom.tplSaveSearchMessage.html(), data)
			$.fancybox 
				content : template
				afterShow : events.setNoTouchMove
				afterClose : events.setTouchMove
			return

		viewSaveSearch : (data)->			
			fn.changeColorButton()
			fn.createPopUp(data)			
			return
		
		hidePopUp : (e) ->
			events.setTouchMove()
			$.fancybox.close()
			dom.box.fadeOut()
			dom.linkSave.removeClass("actived")
			dom.linkSave.addClass("bg_orange")
			return
		showModal : (data)->			
			Sb.trigger("showModalLogin", data)
			return

		viewErrorSearch: (data)->			
			data["tpl"] = "error"
			template = _.template(dom.tplSaveSearchMessage.html(), data)
			$.fancybox 
				content : template
				afterShow : events.setNoTouchMove
				afterClose : events.hidePopUp
			return 
	}   

	fn = {
		configUnderscore : ->
			_.templateSettings =
				evaluate: /\{\{([\s\S]+?)\}\}/g
				interpolate: /\{\{=([\s\S]+?)\}\}/g
			return

		createPopUp : (data)->			
			template = _.template(dom.tplSaveSearch.html(), data)
			$.fancybox
				content : template							
				beforeShow : fn.beforeCreatePopUp
				afterClose : events.hidePopUp
				afterShow : events.setNoTouchMove
			return

		beforeCreatePopUp :->
			Sb.trigger("loadSwitchCheckbox")
			catchDom(st)
			asyncCatchDom()
			asyncSuscribeEvents()
			return

		replaceHtmlBox : (html)->
			$(st.box).remove()
			dom.parent.prepend(html)
			catchDom(st)
			dom.parent.find(dom.box).fadeIn()
			return

		changeColorButton : ()->
			dom.linkSave.removeClass("bg_orange")
			dom.linkSave.addClass("actived")
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		afterCatchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}
,[	"js/dist/libs/fancybox/source/jquery.fancybox.js"
	"js/dist/libs/underscore/underscore.js"
	"js/dist/libs/parsleyjs/dist/parsley.min.js"
    "js/dist/libs/parsleyjs/src/i18n/es.js"
]