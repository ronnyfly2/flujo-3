###
Ordenar por precio, km, año.
@class order_by
@main search/index
@author Jhonnatan Castro
###

yOSON.AppCore.addModule "order_by", (Sb) ->
	defaults = {
		parent      : ".options_filters, .option_filters_mobile"
		el          : "select"
	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.parent = $(st.parent)
		dom.el     = $(st.el, dom.parent)
		return
	suscribeEvents = () ->
		dom.el.on "change", events.search
		return

	events = {
		search : (e) ->
			ele = $(e.target)
			if ele.val() != ""
				window.location = this.value
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}