###
Maneja el callback para ocultar los filtros cuando termino de hacer la transicion de pliegue
@class slider_filters
@main search/index
@author 
###

yOSON.AppCore.addModule "slider_filters", (Sb) ->
    defaults = {
        parent      : ".results"
        el          : ".search_filters"
        filters 	: ".wrapper_filters"
    }

    st = {}
    dom = {}

    catchDom = ->
        dom.parent  = $(st.parent)
        dom.el      = $(st.el, dom.parent)
        dom.filters = $(st.filters, dom.parent)
        return
    suscribeEvents = ->
        dom.parent.on "transitionend", events.animationEnd
        return

    events = {
        animationEnd : (e) ->
             
            if $(".main_search").hasClass("open")
            	$(".wrapper_filters").removeClass("hide")
            else
            	$(".wrapper_filters").addClass("hide")
    }
    initialize = (opts) ->
        st = $.extend({}, defaults, opts)
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }