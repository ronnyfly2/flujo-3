###
Module description
@class valid_year
@main neoauto3/coffee/scripts/modules/all
@author juanpablcs21@gmail.com
###

yOSON.AppCore.addModule "valid_year", (Sb) ->
	defaults = {
		parent      : ".form_year"
		yearDesde   : ".input_year_desde"
		yearHasta   : ".input_year_hasta"
		yearError   : ".error_input_message"
	}
	st = {}

	dom = {}

	catchDom = () ->
		dom.parent 		= $(st.parent)
		dom.yearDesde   = $(st.yearDesde, dom.parent)
		dom.yearHasta   = $(st.yearHasta, dom.parent)
		dom.yearError   = $(st.yearError, dom.parent)
		return
	afterCatchDom = () ->
		fn.registerToParsley()
		return
	suscribeEvents = () ->
		# dom.el.on "click",   events.getAjax
		dom.parent.find('input[type=tel]').on("keyup", events.inputKeyboard)
		return

	events = {
		inputKeyboard: (e) ->
			self = $(e.target)
			textSelf = self.val()
			if(!valid.isNumber(textSelf))
				self.val( textSelf.substring(0, -1) )
			if(dom.yearDesde.val()=="" || dom.yearHasta.val()=="")
				fn.hideError()
			return
	}
	fn = {
		registerToParsley: () ->
			# init plugin
			dom.parent.parsley()
			window.ParsleyValidator.addValidator('validneo', ((value, requirement) ->
				yearDesde = fn.validNumber(dom.yearDesde.val())
				yearHasta = fn.validNumber(dom.yearHasta.val())

				# fix focus
				# setTimeout (->
				# 	if(yearDesde>yearHasta)
				# 		dom.yearHasta.focus()
				# 	else
				# 		dom.yearDesde.focus()
				# 	return
				# ), 200

				if(yearDesde.toString().length==4 && dom.yearHasta.val()=="" || yearHasta.toString().length==4 && dom.yearDesde.val()=="")
					fn.hideError()
					return true

				if(yearDesde.toString().length==4 && yearHasta.toString().length==4)
					if(yearDesde==yearHasta)
						fn.hideError()
						return true
					else if(yearDesde < yearHasta)
						fn.hideError()
						return true
					else
						fn.showError("error: el año no puede ser menor")
						return false
				else
					log yearDesde, "-",yearHasta
					fn.showError("error: año invalido")
					return false

				return false
			))
			
			return
		validNumber: (str) ->
			return if isNaN(parseFloat(str)) then 0 else parseFloat(str)
		hideError: () ->
			dom.yearError.hide()
			dom.yearDesde.removeClass('error')
			dom.yearHasta.removeClass('error')
			return
		showError: (msj) ->
			dom.yearError.show().html(msj)
			dom.yearDesde.addClass('error')
			dom.yearHasta.addClass('error')
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		afterCatchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}
,['js/dist/libs/parsleyjs/dist/parsley.min.js']