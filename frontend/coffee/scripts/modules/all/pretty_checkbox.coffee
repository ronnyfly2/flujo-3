###
Module description
@class pretty_checkbox
@main modules/home/index
@author Jhonnatan Castro
###

yOSON.AppCore.addModule "pretty_checkbox", (Sb) ->
	defaults = {
		parent      : ".box_authorize"
		el          : "label[for=chk_autorize]"
		checkbox 	: ".chk_authorize"
	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.parent 	 = $(st.parent)
		dom.el     	 = $(st.el, dom.parent)
		dom.checkbox = $(st.checkbox, dom.parent)
		return
	suscribeEvents = () ->
		dom.el.on "click", events.checked
		return

	events = {
		checked : (e) ->
			ele = $(e.target)

			if ele.hasClass("checked")
				ele.removeClass("checked")
				dom.checkbox.attr("checked",true)
			else
				ele.addClass("checked")
				dom.checkbox.attr("checked",false)
			
	}	
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}
,[]