###
Registro
@class recaptcha
@main neoauto/ficha-aviso/recaptcha
@author Elizabeth Manrique
###
yOSON.AppCore.addModule "recaptcha", (Sb) ->
    defaults = {
        parent : ".g-recaptcha",
    }
    st = {}
    dom = {}

    catchDom = () ->
        dom.parent          = $(st.parent)
        return
    afterCatchDom = () ->
        # init validation to form register        
        window.parseCaptcha = fn.renderAllReCaptcha        
        return
    suscribeEvents = () ->

        return
    events = {    
                   
    }
    fn = {
        renderAllReCaptcha: () ->                 
            dom.parent.each((index, el) ->                 
                fn.renderReCaptcha($(el))
            )
            return

        renderReCaptcha: (el) ->
            widget_id = window.grecaptcha.render(el[0], {
                'sitekey' : el.attr("data-sitekey"),
                'callback': () ->                    
                    input = el.parents('form').find('input[name^="is_valid_recaptcha_"]')
                    input[0].checked = true
                    input.trigger('change')

            });
            el.attr('data-widget-id', widget_id)

            return

        resetReCaptcha: (el) ->
            input = el.parents('form').find('input[name^="is_valid_recaptcha_"]')
            input[0].checked = false
            input.trigger('change')
            window.grecaptcha.reset(el.attr('data-widget-id'));
            return
    }
    initialize = (opts) ->
        st = $.extend({}, defaults, opts)
        catchDom()
        afterCatchDom()
        Sb.events(["recaptcha:resetReCaptcha"], fn.resetReCaptcha, this)
        Sb.events(["recaptcha:renderReCaptcha"], fn.renderReCaptcha, this)
        return

    return {
        init: initialize
    }
,[
    'https://www.google.com/recaptcha/api.js?onload=parseCaptcha&render=explicit'
]