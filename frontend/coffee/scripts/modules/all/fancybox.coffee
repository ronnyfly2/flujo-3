###
extiende de fancybox. metodos publicos (after, close, ..)
@class fancybox
@main modules/all
@author Juan Pablo Chullo
# ###

yOSON.AppCore.addModule 'fancybox', ((Sb) ->
	dom = {}
	st = 
		fancybox: '.fancybox'
		settings: {}
		settings_fancybox: {}
		idcontent: null
	callbacks = []
	callbacks2 = {}
	catchDom = ->
		dom.fancybox = $(st.fancybox)
		return
	suscribeEvents = ->
		dom.fancybox.on('click', events.fancybox)
		return

	events = {
		fancybox: (e) ->
			e.preventDefault()
			option = fn.getAttributes($(@))
			if(typeof option['data-content-ajax'] != 'undefined')
				st.idcontent = option['data-content-ajax']
				fn.modalBeforeCallback(option)
				st.settings.afterLoad = fn.modalAfterCallback()
				st.settings_fancybox = fn.extendParamsFancyBox(option)
			else
				st.idcontent = option['data-content']
				st.settings.afterLoad = fn.modalAfterCallback()
				st.settings_fancybox = fn.extendParamsFancyBox(option)
				events.modalOpen({})
			return
		modalOpen: (data, id) ->
			id = if typeof id == 'undefined' then st.idcontent else id
			Sb.trigger('underscore:template', id, data, fn.lanzarModalContent)
			return
		callback: ()->
			return
	}
	fn = 
		lanzarModalContent: (html) ->
			st.settings.content = html
			op = $.extend({}, st.settings, st.settings_fancybox)
			$.fancybox( op )
			return html

		modalAfterCallback: () ->
			setTimeout (->
				callbacks2[st.idcontent].after()
				return
			), 700
			return
		modalBeforeCallback: (op) ->
			fn.showLoading()
			callbacks2[st.idcontent].before(op)
			return
		modalClose: () ->
			log "close modal"
			$.fancybox.close()
			return
		showLoading: () ->
			$.fancybox.showLoading()
			return
		hideLoading: () ->
			$.fancybox.hideLoading()
			return
		extendParamsFancyBox: (obj) ->
			newobj = {}
			$.each obj, (key, val)->
				if(key.search(/^fancybox-/g) == -1)
					# delete obj[key]
				else
					newobj[key.replace('fancybox-', '')] = val
				return
			return newobj
		getAttributes: (element) ->
			newobj = {}
			$.each element[0].attributes, (key, attr) ->
				newobj[attr.name] = attr.value
				return
			return newobj
		register: (callback) ->
			c = callback()
			log c
			callbacks2[c.identificator] = c
			return

	initialize = (oP) ->
		st = $.extend({}, st, oP)
		catchDom(st)
		suscribeEvents()
		Sb.events(["fancybox:register"], fn.register, this)

		Sb.events(["fancybox:after"], fn.modalAfter, this)
		Sb.events(["fancybox:before"], fn.modalBefore, this)
		Sb.events(["fancybox:open"], events.modalOpen, this)
		Sb.events(["fancybox:close"], fn.modalClose, this)
		Sb.events(["fancybox:showLoading"], fn.showLoading, this)
		Sb.events(["fancybox:hideLoading"], fn.hideLoading, this)
		return

	return { 
		init: initialize
	}

), [  
	"js/dist/libs/underscore/underscore.js"
    "js/dist/libs/fancybox/source/jquery.fancybox.js"
]