###
Tracking para los formularios
@class tracking_form
@main all
@author Jhonnatan Castro
###
yOSON.AppCore.addModule "tracking_form", (Sb) ->
	
	defaults = {		
		content : ""
		form 	: ""		
	}

	st  = {}    
	dom = {}

	catchDom = (st)->
		dom.content = $(st.content)
		dom.form    = $(st.form, dom.content)
		return

	suscribeEvents = ->
		dom.form.on('submit.tracking', events.tracking)
		return

	events = 
		tracking: (e) ->
			
			mode = dom.form.data("tracking-mode")

			switch mode
				when "search_advanced" then params = fn.captureParamSearchAdvanced()
				when "sticky_newsletter" then params = fn.captureStickyNewsletter()				
			
			Sb.trigger("track", params , e, undefined)
			return
	
	fn = 
		captureParamSearchAdvanced : ->
			paramSend = dom.form.serializeArray()
			
			tipo 	= valid.isEmpty(paramSend[0].value) 
			estado 	= valid.isEmpty(paramSend[1].value, "todos")
			marca 	= valid.isEmpty(paramSend[2].value, "no-marca")
			modelo 	= valid.isEmpty(paramSend[3].value, "no-modelo")

			["buscar", "portada", marca+"_"+modelo+"_"+tipo+"_"+estado]			

		captureStickyNewsletter : ->
			
			paramSend = dom.form.serializeArray()

			option = if (paramSend[1].value is "on") then "si_opt_in" else "no_opt_in"
			
			["suscribirse", "suscripcion_home", option]			

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		suscribeEvents()
		return

	return { 
		init: initialize
	}