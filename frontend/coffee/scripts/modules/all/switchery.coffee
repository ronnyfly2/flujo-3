###
Module description
@class switchery
@main anunciante/publicacion
@author Ronny Cabrera
###

yOSON.AppCore.addModule "switchery", (Sb) ->
	defaults = {
		switchCheckbox : ".switchery"
	}
	st = {}
		
	dom = {}

	catchDom = () ->
		dom.switchCheckbox = $(st.switchCheckbox)
		return
	suscribeEvents = () ->
		dom.switchCheckbox.on "change", fn.onChange
		fn.initSwitchCheckbox()
		return
	
	fn = {
		initSwitchCheckbox : () ->
			# dom.switchCheckbox.each ->
			# 	$(this).switcher()
			# 	return
			elems = document.querySelectorAll(st.switchCheckbox)
			i = 0
			while i < elems.length
				new Switchery(elems[i])
				i++
			return

		onChange: ()->
			log "cambiando combo:", $(@).val()
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		suscribeEvents()
		Sb.events(["loadSwitchCheckbox"], fn.initSwitchCheckbox, this)	
		return
	return {
		init: initialize
	}
,['js/dist/libs/switchery/switchery.js']