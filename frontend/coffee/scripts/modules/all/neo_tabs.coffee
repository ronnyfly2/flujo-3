###
buscador autocompletador para neoauto
@class tabs
@main neoauto/all
@author Juan Pablo Chullo feat Ronny Cabrera
###
yOSON.AppCore.addModule "neo_tabs", (Sb) ->
	defaults = {
		parent          : ".neo_tabs"
		tabOption       : ".tab_option a"
		tabContent      : ".tab_content"
		active          : ".active"
	}

	st = {}
	dom = {}
	handler = {}

	catchDom = () ->
		dom.parent      = $(st.parent)
		dom.tabOption   = $(st.tabOption, dom.parent)
		dom.tabContent  = $(st.tabContent, dom.parent)
		return 
	afterCatchDom = () ->
		fn.validationTouch()
		return
	suscribeEvents = () ->
		dom.tabOption.on(handler, events.tabAction)
		return

	events = {
		tabAction: (e) ->
			e.preventDefault()
			fn.action($(@))
			return
	}
	fn = {
		action: (self) ->
			selector = self.data('tabclass')
			# controla tab action
			fn.tabToggle(self)
			# controla tab content
			fn.tabToggle($(self.parents('.neo_tabs').find('.' + selector)))
			# lanza lazyload
			Sb.trigger("runLazyLoad")
			# callback extra
			events.callbackTab(selector)
			return

		tabToggle: (element) ->
			element.siblings().removeClass(fn.removePoint(st.active))
			element.addClass(fn.removePoint(st.active))
			return
	 
		removePoint: (string) ->
			return string.replace(/\./g, '', string)
		setCallbackTab: (fn) ->
			events.callbackTab = fn
			return
		validationTouch : ->
			if isMobile.any()
				handler = "touchend"
			else
				handler = "click"
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		afterCatchDom()
		suscribeEvents()
		Sb.events(["tabCallback"], fn.setCallbackTab, this)
		log "script neo_tabs"
		return

	return {
		init: initialize
	}
,[]
