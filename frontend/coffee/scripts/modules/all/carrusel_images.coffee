###
Añade la funcionalidad slider para la seccion resultado de busqueda y detalle
@class carrusel_images
@main modules/all
@author Jhonnatan Castro Vilela
###

yOSON.AppCore.addModule "carrusel_images", (Sb) ->
	defaults = {
		parent  	: ".item_images"
		el 			: ".slider"
		slick 		:
			default :
				speed 		: 300
				infinite 	: true
				dots 		: false
	}
	st = {}
	dom = {}

	catchDom = (st) ->
		dom.parent 	= $(st.parent)
		dom.el 		= $(st.el, dom.parent)
		dom.prev 	= $(st.prev)
		dom.next 	= $(st.next)
		return

	afterCatchDom = ()->
		fn.loadSlick()
		return

	fn = {
		loadSlick : (slider, callback)->
			log "run slick"
			config = $.extend({}, st.slick.default, slider.config)
			slider.element.slick(config)
			if (typeof callback == "function")
				callback(slider)
			return
	}

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)		
		Sb.events(["carrusel_images:loadSlick"], fn.loadSlick, this)		
		return

	return {
		init: initialize
	}
,["js/dist/libs/slick-carousel/slick/slick.min.js"]