###
buscador autocompletador para neoauto
@class search
@main neoauto/all
@author Juan Pablo Chullo
###
yOSON.AppCore.addModule "search_top", (Sb) ->
  defaults = {
    document           : document
    parent             : ".frm_search"
    inputQuery         : ".quickSearch"
    appendAutocomplete : ".append_autocomplete"
    loaderIcon         : ".loader_icon"
    btnSearch          : "button.search"
  }
  st = {}
  dom = {}
  urlAjax = yOSON.url.base + "/ajax/search/autocomplete/text/[search]"
  dragging = false

  catchDom = () ->
    dom.document            = $(st.document)
    dom.parent              = $(st.parent)
    dom.inputQuery          = $(st.inputQuery, dom.parent)
    dom.loaderIcon          = $(st.loaderIcon, dom.parent)
    dom.btnSearch           = $(st.btnSearch, dom.parent)
    return
  afterCatchDom = () ->
    fn.initAutocomplete(dom.inputQuery)
    dom.inputQuery.on('focus', events.focusInput)
    return

  suscribeEvents =  ->
    dom.document.on "touchstart", events.touchStart
    dom.document.on "touchmove", events.touchMove
    dom.document.on "touchend", events.touchEnd
    dom.inputQuery.on("keydown", events.keydown)
    return

  events = {
    keydown : (e)->
      unicode = undefined
      unicode = if e.keyCode then e.keyCode else e.charCode
      if unicode == 49 or unicode == 106
        e.preventDefault()
      return

    touchEnd : (e) ->
      if(!dragging) 
        target = e.target
        if fn.isTargetNotExistsInElement(target, st.parent)        
          events.hidePopupSearch()        
      return
    touchStart: (e) ->
      dragging = false
      return
    touchMove: (e) ->
      dragging = true
      return

    focusInput: (e) ->
      if($(@).val().length>2)
        ### tech para colocar puntero al final ###
        fn.pointerMouseToFinal($(@))
        ### end ###
        $('.ui-autocomplete').show()        
      return

    hidePopupSearch: () ->      
      #$(".logo").hide()
      dom.inputQuery.autocomplete('close')
      dom.inputQuery.blur()
      return
  }
  fn = {
    isTargetNotExistsInElement : (target, element) ->     
      if $(target).closest(element).length == 0       
        true  

    pointerMouseToFinal: (el) ->
      text = el.val()
      el.val("")
      setTimeout(()->
        el.val(text)
      ,50)
      return
    initAutocomplete: (element) ->
      # init plugin
      element.autocomplete
        minLength: 3
        appendTo: element.siblings(st.appendAutocomplete)
        search: ()->
          fn.showLoader()
          return
        open: ()->
          $('.ui-autocomplete').off('menufocus')
          fn.hideLoader()
          return
        close: ()->
          fn.hideLoader()
          return
        source: (request, response) ->
          url = urlAjax.replace('[search]',request.term)
          fn.getAjax(url, response)
          return
        focus: () ->
          return false
        select: (event,ui) ->
          this.value = ui.item.label.replace(new RegExp('<strong>','g'),'').replace(new RegExp('</strong>','g'),'')
          fn.redirectResult(ui.item.value)
          return false
      .data( "ui-autocomplete" )._renderItem = ( ul, item ) ->
          return $( "<li>" )
            .attr( "data-value", item.value )
            .append( item.label )
            .appendTo( ul )
        # end plugin
      return  
    redirectResult : (url) ->
      # alert "ir a " + url
      window.location.href = url
      return
    hideLoader: () ->
      dom.loaderIcon.hide()
      dom.btnSearch.show()
      return
    showLoader: () ->
      dom.loaderIcon.show()
      dom.btnSearch.hide()
      return
    getAjax : (url, callback) ->
      $.ajax
        url: url
        dataType: 'json'
        success: (result) ->
          data = result.data
          if(data.length<1)
            fn.hideLoader()
                      
          list = fn.prepareData(data)
          callback(list)

          return
        error: (data) ->
          log "error ajax search"
          fn.hideLoader()
          return
      return
    prepareData : (data)->      
      $.map(data, fn.createOption)      

    createOption : (item)->
      option =
        label: item.suggestionsMatch.replace(new RegExp('\\*\\*','g'), '<strong>').replace(new RegExp('\!\!','g'),'</strong>'),
        value: yOSON.url.search + item.slugModeloSearch
      log option
      option
  }
  initialize = (opts) ->
    st = $.extend({}, defaults, opts)
    catchDom()
    afterCatchDom() 
    suscribeEvents() 
    #Sb.trigger("setCallbackTouchend", element: st.parent , events.hidePopupSearch)
    log "script search_top"
    return

  return {
    init: initialize
  }
,["js/dist/libs/jquery-ui/jquery-ui.min.js"]