###
Module que cierra cualquier elemento cuando se da touch al document
@class touch_close_element
@main modules/all
@author 
###

yOSON.AppCore.addModule "touch_close_element", (Sb) ->
	defaults = {
		document 	: document
		element		: ".element"
	}

	st = {}
	dom = {}
	dragging = false
	listCallbacks = []

	catchDom = (st) ->
		dom.document= $(st.document)
		return

	suscribeEvents =  ->
		dom.document.on "touchstart", events.touchStart
		dom.document.on "touchmove", events.touchMove
		dom.document.on "touchend", events.touchEnd
		return

	events = {		
		
		touchEnd : (e) ->
			if(!dragging)	
				target = e.target
				if fn.isTargetNotExistsInElement(target, st.element)				
					fn.loadCallbacks()				
			return
		touchStart: (e) ->
			dragging = false
			return
		touchMove: (e) ->
			dragging = true
			return
	}
	fn = {
		loadCallbacks : ->
			$.each(listCallbacks , fn.callback)			
			return

		callback : (i, fn)->
			fn()
			return

		isTargetNotExistsInElement : (target, element) ->			
			if $(target).closest(element).length == 0				
				true	

		setCallbackTouchend : (opts, callbackDeal) ->			
			st = $.extend({}, defaults, opts)
			listCallbacks.push(callbackDeal)
			return
	}

	initialize = (opts)->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		Sb.events(["setCallbackTouchend"], fn.setCallbackTouchend, this)		
		suscribeEvents()
		return

	return {
		init: initialize
	}