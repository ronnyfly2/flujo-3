###
Carga asincrona para el slider de imagenes en busqueda
@class loader_slider_images
@main modules/all
@author Jhonnatan Castro Vilela
###

yOSON.AppCore.addModule "loader_slider_images", (Sb) ->
	defaults = {
		slider 		: ".slider"
		parent      : ".item_images"
		el          : ".item"
		arrows      : ".arrows"
		tpl         : "#tplItemSlider"
		urlAjax     : yOSON.url.base + "/ajax/search/images/"
		prev 		: ".icon_arrow_left"
		next 		: ".icon_arrow_right"
		slickDots 	: ".slick-dots"		
	}

	st = {}
	dom = {}
	ajax = []
	images = []
	baseImage = ""
	catchDom = (st) ->
		dom.parent  = $(st.parent)
		dom.el      = $(st.el, dom.parent)
		dom.arrows      = $(st.arrows, dom.parent)
		dom.tpl     = $(st.tpl)
		return

	afterCatchDom = ->
		_.templateSettings =
			evaluate: /\{\{([\s\S]+?)\}\}/g
			interpolate: /\{\{=([\s\S]+?)\}\}/g
		return

	suscribeEvents = () ->
		dom.parent.hover events.onFigure
		return

	events = 
		onFigure : (e) ->
			ele = $(@)			
			if ele.find(st.slider).length > 0
				if not ele.hasClass("loaded")
					id = ele.find(st.slider).data('slug')
					baseImage = ele.find(st.slider).data("baseimage")
					xhr = ajax[id]
					fn.validAjax(xhr)
					fn.loadAjax(id, ele)

	fn = {

		validAjax : (xhr)->
			if xhr && xhr.readyState != 4
				xhr.abort()
			return

		loadAjax : (id, ele) ->			
			params = idAviso : id			
			ajax[id] = $.ajax
				data        : params
				method      : "POST"
				url         : st.urlAjax
				beforeSend  : ->
					fn.loadingAjax(ele)
					return
				success     : (result)->
					fn.successAjax(result, ele)
					return
				dataType    : "json"
			return

		loadingAjax : (ele)->
			ele.addClass("loading")
			return

		successAjax : (result, ele)->
			result.data.baseImage = yOSON.eHost + baseImage
			content = ele.find(st.el)
			ele.addClass("loaded").removeClass("loading")

			result['url'] = content.find("a").attr("href")

			if result.status
				html = _.template(dom.tpl.html(), result)

				content.after(html)

				slider = ele.find(st.slider)
				sliderItems = slider.find(".item")
				
				fn.prepareImages(sliderItems)

				log "images to load", images

				ele = slider.closest(st.parent)

				beforeLoadImages
					images : images
					MaxNumberImagesLoad : images.length
					callback : ->
						Sb.trigger("carrusel_images:loadSlick", {
							element:slider
							config : 													
								prevArrow : ele.find(st.prev)
								nextArrow : ele.find(st.next)
						})
						return
				images = []

			return

		prepareImages : (slider)->
			$(slider).each( (index, ele)->
				images.push($(ele).find("img").attr("src"))
				return
			)
			return
	}

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		afterCatchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}
,["js/dist/libs/underscore/underscore.js"]