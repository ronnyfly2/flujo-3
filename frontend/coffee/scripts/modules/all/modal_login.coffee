###
Module modal de login
@class modal_login
@main modules/all
@author Jhonnatan Castro
###

yOSON.AppCore.addModule "modal_login", (Sb) ->
    defaults = {
        parent      : "header"                
        linkDesktop : ".login_link.modal a"
        loginSocialNetworks : ".login_social_networks"
        
        form        : ".frm_login"
        txtEmail    : "input[name=email]"
        txtPassword : "input[name=password]"
        txtToken    : "input[name=token]"
        ajaxLogin   : "/cuenta/login"
        message     : ".message"

        tplLogin    : "#tplLogin"
    }
    st = {}
    dom = {}
    icons = {}

    catchDom = (st) ->
        dom.parent = $(st.parent)        
        dom.linkDesktop = $(st.linkDesktop, dom.parent)
        dom.loginSocialNetworks = $(st.loginSocialNetworks)
        dom.iconsSocialNetworks = dom.loginSocialNetworks.find("a")
        dom.tplLogin = $(st.tplLogin)
        return

    suscribeEvents = () ->
        dom.linkDesktop.on "click", events.clickModalLogin
        return

    asynCatchDom = ->
        dom.form        = $(st.form)
        dom.txtEmail    = $(st.txtEmail, dom.form)
        dom.txtPassword = $(st.txtPassword, dom.form)
        dom.message     = $(st.message, dom.form)
        dom.txtToken    = $(st.txtToken, dom.form)
        return

    asynSuscribeEvents = ->        
        dom.form.parsley()
        dom.form.on("submit", events.submitFormLogin)
        return

    events = {
        clickModalLogin : (e) ->            
            fn.showModalLogin()
            return

        submitFormLogin : (e)->
            dom.form.parsley().validate()
            form = $(e.target)
            if (form.parsley().isValid())
                if form.data("submit") == "ajax"
                    $.ajax
                        type    : form.attr("type")
                        url     : form.attr("action")
                        data    : form.serialize()
                        beforeSend : $.fancybox.showLoading
                        success : fn.loadFormLogin
                    e.preventDefault()
            return   
    }   

    fn = {
        loadFormLogin : (data)->
            $.fancybox.hideLoading()
            if data.ajax is true
                window.location.href = data.url
            else
                dom.message.text(data.msg)
                dom.txtToken.val(data.token)
            return

        showModalLogin : (data)->
            log data
            template = _.template(dom.tplLogin.html(), data)            
            $.fancybox
                afterShow : fn.afterShowModal
                content : template
            return

        createModalTemplate : (e)->
            log 'template'
            return
        afterShowModal : ->
            fn.reloadIconsSocials()
            asynCatchDom()
            asynSuscribeEvents()
            return

        reloadIconsSocials : ->
            icons = dom.iconsSocialNetworks
            dom.iconsSocialNetworks.remove()
            setTimeout( fn.loadIcons ,1)
            return
        loadIcons : ->            
            dom.loginSocialNetworks.html(icons)            
            dom.loginSocialNetworks.find("a").fadeIn(1)
            return
    }

    initialize = (opts) ->
        st = $.extend({}, defaults, opts)
        catchDom(st)
        Sb.events(["showModalLogin"], fn.showModalLogin, @)
        suscribeEvents()        
        return

    return {
        init: initialize
    }
,[  "js/dist/libs/underscore/underscore.js"
    "js/dist/libs/fancybox/source/jquery.fancybox.js"
    "js/dist/libs/parsleyjs/dist/parsley.min.js"
    "js/dist/libs/parsleyjs/src/i18n/es.js"
]