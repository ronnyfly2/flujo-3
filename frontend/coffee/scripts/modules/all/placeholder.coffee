###
Agrega funcionalidad de placeholder a navegadores que no lo soporten
@class placeholder
@main neoauto/all
@author Jhonnatan Castro && Juan Pablo
###
yOSON.AppCore.addModule "placeholder", (Sb) ->
  st =
      inputs    : "input, textarea"

  dom = {}
  catchDom = ->
    dom.inputs = $(st.inputs)
    return
  suscribeEvents = ->
    if $.browser.msie
      dom.inputs.on "focusout", events.inputFocusOut 
    return
  events = 
    inputFocusOut : (e) ->
      ele = $(e.target)
      if(ele.attr('placeholder'))
        if(ele.val()=="")
          ele.removeClass('unfocus')
        else
          ele.addClass('unfocus')
      return
  functions = 
    enablePlaceholder : ->
      if $.browser.msie
        dom.inputs.placeholder()
        log "ready placeholder"
      return


  init: ->
    catchDom()
    suscribeEvents()
    functions.enablePlaceholder()
    Sb.events(["reloadPlaceholder"], functions.enablePlaceholder, this)
    return

, ['js/dist/libs/jquery-placeholder/jquery.placeholder.js']
