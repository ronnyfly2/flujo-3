###
Module para realizar touch a dispositivos moviles
@class touch_menu_principal
@main modules/all
@author 
###

yOSON.AppCore.addModule "touch_menu_principal", (Sb) ->
	defaults = {		
		document    : document
		parent 		: ".links_menu"
		el 			: ".links_menu > li:last > a"
		subMenu 	: ".submenu"
	}

	st = {}
	dom = {}
	dragging = false
	catchDom = (st) ->
		dom.document = $(st.document)
		dom.parent 	 = $(st.parent)
		dom.el     	 = $(st.el)
		dom.subMenu  = $(st.subMenu, dom.parent)		
		return

	afterCatchDom = ->
		fn.validateTouchDevice()		
		return

	suscribeEvents = ->
		dom.el.on "touchend", events.showSubMenu	
		dom.document.on "touchend", events.touchEnd
		return

	events = {
		touchEnd : (e) ->
			target = e.target
			if fn.isTargetNotExistsInElement(target, st.parent)        
				events.hideSubmenu()			
			return

		showSubMenu : (e) ->
			e.stopPropagation()
			dom.el.addClass("active")
			dom.subMenu.toggle()
			return

		hideSubmenu : ->
			dom.subMenu.hide()
			dom.el.removeClass("active")
			return
		
		registerTouchend : ->
			#Sb.trigger("setCallbackTouchend", element: st.subMenu , events.hideSubmenu)
			return
	}

	fn = {
		isTargetNotExistsInElement : (target, element) ->     
			if $(target).closest(element).length == 0       
				true

		validateTouchDevice : ->
			if isMobile.any()
				fn.changeCursorPointer(dom.el)
				events.hideSubmenu()
				events.registerTouchend()
			return
		
		changeCursorPointer : (ele)->
			ele.css
				cursor : "inherit"			
			return			
	}

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		afterCatchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}