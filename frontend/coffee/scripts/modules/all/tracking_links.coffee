###
Tracking para los enlaces
@class tracking_links
@main all
@author Jhonnatan Castro
###
yOSON.AppCore.addModule "tracking_links", (Sb) ->
	
	defaults = params : []

	st  = {}
	dom = {}

	catchDom = (st)->
		dom.content = $(st.content)
		dom.link    = $(st.link, dom.content)
		dom.params 	= st.params
		return

	suscribeEvents = ->
		dom.link.on('click', events.tracking)
		return

	events = 
		tracking: (e) ->
			ele = $(e.target)

			if !ele.hasClass("track")
				ele = ele.parents(".track")
			
			if ele.prop("tagName").toLowerCase() == 'a'
				target = ele.attr("target")

				callback = if target is "_blank" then undefined else goToURL

			unless ele.data("track") is undefined
				text = ele.data("track")
				dom.params.push(text)

			Sb.trigger("track", dom.params, e, callback)
			dom.params.pop()
			return

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		suscribeEvents()
		return

	return { 
		init: initialize
	}