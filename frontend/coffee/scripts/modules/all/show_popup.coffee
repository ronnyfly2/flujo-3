###
Agrega funcionalidad popup verified visa
@class show_popup
@main modules/all
@author Jhonnatan Castro
###

yOSON.AppCore.addModule 'show_popup', ((Sb) ->
	dom = {}
	st = 
		btnVerifyByVisa: '.show-popup'

	catchDom = ->
		dom.btnVerifyByVisa = $(st.btnVerifyByVisa)
		return

	suscribeEvents = ->
		dom.btnVerifyByVisa.on('click', events.showPopup)
		return

	events = 
		showPopup: (e) ->
			e.preventDefault()
			href = dom.btnVerifyByVisa.attr('href')
			fn.createPopUp(href, 606, 405, 'no')
			return

	fn = 
		createPopUp: (pagina, ancho, alto, barras, tit) ->		
			izquierda = if screen.width then (screen.width - ancho) / 2 else 100
			arriba = if screen.height then (screen.height - alto) / 2 else 100
			opciones = 'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=' + barras + ',resizable=0,width=' + ancho + ',height=' + alto + ',left=' + izquierda + ',top=' + arriba + ''
			tit = 'nombre_' + ancho + 'x' + alto
			window.open(pagina, tit, opciones)
			return

	initialize = (oP) ->
		st = $.extend({}, st, oP)
		catchDom(st)
		suscribeEvents()
		return

	return { 
		init: initialize
	}

), []
