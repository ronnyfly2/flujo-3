###
Precarga las imagenes de lazyload
@class lazyload
@main modules/all
@author 
###

yOSON.AppCore.addModule "lazyload", (Sb) ->
	defaults = {
		imgLazy     : "img.lazy"
	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.imgLazy  = $(st.imgLazy)
		return
	afterCatchDom = ->
		dom.imgLazy.show().lazyload()
		return
	
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()		
		afterCatchDom()
		Sb.events(["runLazyLoad"],afterCatchDom,this)
		return

	return {
		init: initialize
	}
,["js/dist/libs/jquery-lazyload/lazyload.js"]