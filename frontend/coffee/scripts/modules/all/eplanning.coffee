###
Precarga las imagenes de eplanning
@class eplanning
@main modules/all
@author Juan Pablo Chullo
###

yOSON.AppCore.addModule "eplanning", (Sb) ->
	defaults = {
		eplanning     : ".eplanning"
	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.eplanningClass  = $(st.eplanning)
		return
	afterCatchDom = ->
		dom.eplanningClass.each () ->
			fn.eplanningEach($(@))
			return
		return
	fn = {
		eplanningEach: (el) ->
			# si activan eplanning para mobile
			# quitar esta condicional
			if(isMobile.any()==null)
				eID = el.data('eplanningid')
				ePos = el.data('eplanningposition')

				if(ePos)
					firstCnt = el.find(".tab_contents>div").first()
					fn.eplanningTemporal(firstCnt, eID, ePos)
				else
					fn.eplanningHtml(el, eID)

				fn.eplanningDependence(eID)
			return
		eplanningHtml: (el, eID) ->
			html = "<div class='eplAds' id='eplAdDiv"+eID+"'></div>"
			el.append(html)
			return
		eplanningDependence: (eID) ->
			eplanningDependence (loader) ->
				log loader
				if(loader())					
					document.epl.showSpace(eID)
				else
					setTimeout(loader,250)
				return
			return
		eplanningTemporal: (el, id, pos) ->
			self = el.find("li").eq(pos-1)
			fn.eplanningHtml(self, id)
			return
		tabCallback: (a) ->
			log a
			cnt = $(a).closest("section")
			if(cnt.hasClass("eplanning"))
				eID = cnt.data('eplanningid')
				ePos = cnt.data('eplanningposition')

				cnt.find(".eplAds").remove();
				fn.eplanningTemporal($(a), eID, ePos)
				document.epl.reloadSpace(eID)
			return

	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		afterCatchDom()
		# Sb.trigger("tabCallback", fn.tabCallback)
		log "eplanning"
		return

	return {
		init: initialize
	}
# ,["js/dist/libs/eplanning/eplanning.js"]