###
event click open menu and close
@class mobile_menu
@main neoauto/all
@author Juan Pablo Chullo
###
yOSON.AppCore.addModule "mobile_menu", (Sb) ->
  defaults = {
    parent              : ".header_mobile"
    linkLogin           : ".menu .icon_user"
    linkServices        : ".menu .icon_nav_menu"

    menuMobile           : ".menu_mobile"
    active              : ".active"

    sections            : "body > section, body > footer, body > main"

    contentModal        : "#box_login"

    # captura links para crear el menu mobile
    linkSellVehicle     : "#btnPublishAd"
    linksMenuOne        : ".links_menu > li"
    linksMenuTwo        : ".link_externals > li"
  }
  st = {}
  dom = {}
  links_array = []

  catchDom = () ->
    dom.parent            = $(st.parent)
    dom.menuMobile        = $(st.menuMobile)

    dom.linkLogin         = $(st.linkLogin, dom.parent)
    dom.linkServices      = $(st.linkServices, dom.parent)

    dom.sections          = $(st.sections)
    dom.window            = $(window)
    dom.contentModal      = $(st.contentModal)

    dom.linkSellVehicle   = $(st.linkSellVehicle).clone()
    dom.linksMenuOne      = $(st.linksMenuOne).clone()
    dom.linksMenuTwo      = $(st.linksMenuTwo).clone()
    return

  afterCatchDom = () ->
    # logica
    fn.prepareMenu()
    fn.createMenu()
    return

  suscribeEvents = () ->
    # dom.linkLogin.on("click", fn.tooglePageLogin)
    dom.linkServices.on("click", fn.navToggle)
    dom.menuMobile.on("click", ".openMenuServices", events.subMenuToggle)

    # fixed para los que hacen resize del navegador para ver el efecto responsive
    dom.window.on('resize', events.resize)
    return

  events = {   
    
    subMenuToggle: (e) ->
      e.preventDefault()
      $(@).parent().find('ul').slideToggle('fast')
      return

    resize: (e) ->      
      currentWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      
      if(currentWidth>670)        
        #fn.closePageLogin(dom.linkLogin)
        fn.closeMenu(dom.linkServices)      
      #else        
        #if typeof $.fancybox != "undefined"
          #$.fancybox.close()
      return
  }
  fn = {
    createMenu: () ->
      dom.menuMobile.html("<ul>" + links_array.join('') + "</ul>")
      dom.menuMobile.find("li:lt(5)").addClass('bg_pattern_diamond')
      return

    prepareMenu:() ->
      # trae links clonados y prepara en un array
      dom.linkSellVehicle.removeAttr('id class data-title_mobile').text(dom.linkSellVehicle.data('title_mobile'))
      links_array.push("<li>" + dom.linkSellVehicle[0].outerHTML + "</li>")

      dom.linksMenuOne.each(fn.prepareMenuLinks)

      dom.linksMenuTwo.each(fn.prepareMenuLinks)
      return

    prepareMenuLinks: (i,el) ->
      $(el).find('a').removeClass('btn btn_diagonal').end()
      links_array.push(el.outerHTML)
      return

    navToggle: (e) ->
      e.preventDefault()
      ele = $(e.target)
      if(ele.hasClass(fn.removePoint(st.active)))
        fn.closeMenu(ele)
      else
        fn.openMenu(ele)
      return

    openMenu: (ele) ->
      dom.menuMobile.show()
      ele.addClass(fn.removePoint(st.active))
      dom.linkLogin.removeClass("active")
      dom.contentModal.hide()
      fn.hideAllSections()
      return

    closeMenu: (ele) ->
      dom.menuMobile.hide()
      ele.removeClass(fn.removePoint(st.active))
      fn.showAllSections()
      return

    removePoint: (string) ->
      return string.replace(/\./g, '', string)

    tooglePageLogin : (e)->
      e.preventDefault()
      ele = $(e.target)

      if ele.hasClass("active")
        fn.closePageLogin(ele)
      else
        fn.openPageLogin(ele)

      return

    openPageLogin : (ele)->
      fn.hideAllSections()
      dom.menuMobile.hide()
      dom.contentModal.show()
      ele.addClass("active")
      dom.linkServices.removeClass("active")
      return
      
    closePageLogin : (ele)->
      fn.showAllSections()
      
      if typeof $.fancybox != "undefined"
        if not $.fancybox.isOpen
          dom.contentModal.hide()
      
      ele.removeClass("active")
      return  

    hideAllSections : ->
      dom.sections.not(".login_your_account").addClass('hide')
      return
    
    showAllSections : ->
      dom.sections.removeClass('hide')
      return    
  }
  initialize = (opts) ->
    st = $.extend({}, defaults, opts)
    catchDom()
    afterCatchDom()
    suscribeEvents()
    log "script mobile_menu"
    return

  return {
    init: initialize
  }
,[]
