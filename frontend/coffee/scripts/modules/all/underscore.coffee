###
extiende del underscore _
@class underscore
@main flux/all
@author Juan Pablo
###
yOSON.AppCore.addModule "underscore", (Sb) ->
	_.templateSettings =
				evaluate: /\{\{([\s\S]+?)\}\}/g
				interpolate: /\{\{=([\s\S]+?)\}\}/g

	fn =
		init : ->
			log ""
			return
		template: (id, data, fn) ->
			data = if typeof data == 'undefined' then {} else data
			html = _.template($(id).html(), data)
			fn(html)
			return

	initialize = (oP) ->
		$.extend oP
		Sb.events(['underscore:template'], fn.template, this)
		return

	return {
		init: initialize
	}
,["js/dist/libs/underscore/underscore.js"]