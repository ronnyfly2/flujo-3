###
Module modal de login
@class login
@main modules/all
@author Jhonnatan Castro && Juan pablo
###

yOSON.AppCore.addModule "login", (Sb) ->
	defaults = {
		parent      : "header"
		loginSocialNetworks : ".login_social_networks"
		
		form        : ".frm_login"
		txtEmail    : "input[name=email]"
		txtPassword : "input[name=password]"
		txtToken    : "input[name=token]"
		ajaxLogin   : "/cuenta/login"
		message     : ".message"

		tpl 	    : "#tplLogin"
		url_token   : "/ajax/common/get-token/"
	}
	st = {}
	dom = {}
	icons = {}

	catchDom = (st) ->
		dom.parent = $(st.parent)        
		dom.loginSocialNetworks = $(st.loginSocialNetworks)
		dom.iconsSocialNetworks = dom.loginSocialNetworks.find("a")
		return

	suscribeEvents = () ->
		return

	asynCatchDom = ->
		dom.form        = $(st.form)
		dom.txtEmail    = $(st.txtEmail, dom.form)
		dom.txtPassword = $(st.txtPassword, dom.form)
		dom.message     = $(st.message, dom.form)
		dom.txtToken    = $(st.txtToken, dom.form)
		return

	asynSuscribeEvents = ->        
		dom.form.parsley()
		dom.form.on("submit", events.submitFormLogin)
		return

	events = {
		submitFormLogin : (e)->
			dom.form.parsley().validate()
			form = $(e.target)
			if (form.parsley().isValid())
				if form.data("submit") == "ajax"
					$.ajax
						type    : form.attr("type")
						url     : form.attr("action")
						data    : form.serialize()
						beforeSend : $.fancybox.showLoading
						success : fn.loadFormLogin
					e.preventDefault()
			return   
	}   

	fn = {
		loadFormLogin : (data)->
			$.fancybox.hideLoading()
			if data.ajax is true
				window.location.href = data.url
			else
				dom.message.text(data.msg)
				dom.txtToken.val(data.token)
			return

		beforeModal : (id)->
			log id
			fn.ajaxToken (json) ->
				data = {token:json.data}
				Sb.trigger('fancybox:open', data)
				return
			return
		afterModal : ->
			fn.reloadIconsSocials()
			asynCatchDom()
			asynSuscribeEvents()
			return


		ajaxToken: (callback)->
			$.ajax
				type    : "GET"
				url     : st.url_token
				success : callback
			return

		reloadIconsSocials : ->
			icons = dom.iconsSocialNetworks
			dom.iconsSocialNetworks.remove()
			setTimeout( fn.loadIcons ,1)
			return
		loadIcons : ->            
			dom.loginSocialNetworks.html(icons)            
			dom.loginSocialNetworks.find("a").fadeIn(1)
			return
		register: () ->
			return {identificator:st.tpl, after:fn.afterModal, before:fn.beforeModal}
	}

	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom(st)
		Sb.trigger 'fancybox:register',fn.register
		Sb.events(['login:asyncDom'], fn.afterModal, this)
		# Sb.trigger 'fancybox:after', fn.syncDom2
		# Sb.trigger 'fancybox:before', fn.beforeModal
		suscribeEvents()        
		return

	return {
		init: initialize
	}
,[  "js/dist/libs/underscore/underscore.js"
	"js/dist/libs/fancybox/source/jquery.fancybox.js"
	"js/dist/libs/parsleyjs/dist/parsley.min.js"
	"js/dist/libs/parsleyjs/src/i18n/es.js"
]