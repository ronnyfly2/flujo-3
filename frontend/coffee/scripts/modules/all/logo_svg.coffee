###
Precarga Logo SVG
@class logo_svg
@main modules/all
@author 
###

yOSON.AppCore.addModule "logo_svg", (Sb) ->
	defaults = {
		imgLogo     : ".logo_neoauto"
	}
	st = {}
	dom = {}

	catchDom = () ->
		dom.imgLogo  = $(st.imgLogo)
		return
	afterCatchDom = ->
		if($.browser.version > 9 || $.browser.msie==false)
			fn.ajaxLoad(dom.imgLogo)
		return
	fn = {
		ajaxLoad: (el) ->
			$.ajax
				url : el.data('svg')
				cache: true
				dataType: 'html'
				success: (result) ->
					# el.replaceWith(result)
					el.attr('src',el.data('svg')).removeAttr('data-svg')
					return
			return
	}
	initialize = (opts) ->
		st = $.extend({}, defaults, opts)
		catchDom()
		afterCatchDom()
		return

	return {
		init: initialize
	}
,[]