###
Modulo madre para los tracking
@class tracking
@main all
@author Jhonnatan Castro
###
yOSON.AppCore.addModule "tracking", (Sb) ->
    
    defaults = {
        content : "body"
        ele     : ".track"
    }

    debug = false

    st  = {}    
    dom = {}

    catchDom = (st)->
        dom.content = $(st.content)
        dom.ele     = $(st.ele, dom.content)
        return

    afterCatchDom = (st)->
        fn.showTrackings(st)
        fn.windowDebug()
        return

    fn = 
        showTrackings : (st)->
            if debug
                ele = dom.ele      
                ele.addClass("debug")
            return

        hideTrackings : ->
            ele = dom.ele      
            ele.removeClass("debug")
            return

        prepareParamsTracking : (params)->
            paramsTracking = []  
            paramsTracking.push('send', 'event')

            i = 0
            while i < params.length
                paramsTracking.push(params[i])
                i++
            paramsTracking
        
        track : (array, event, fnCallback)->

            params = fn.prepareParamsTracking(array)
            
            if !debug
                if typeof fnCallback == "function"
                    event.preventDefault()
                    params.push(
                       hitCallback : fnCallback(event)
                    )
            if debug
                event.preventDefault()

            ga.apply(this, params)
            log "analytics sending : ",params
            return

        windowDebug : ->
            window.tracking = {                
                debug : (state)->
                    debug = state
                    if state                            
                        fn.showTrackings()
                    else
                        fn.hideTrackings()
                    return
            }
            return
       
    initialize = (opts) ->
        st = $.extend({}, defaults, opts)
        catchDom(st)
        Sb.events(["track"], fn.track, this)
        afterCatchDom()
        return

    return { 
        init: initialize
    }