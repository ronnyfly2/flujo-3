- Aqui van en carpetas los nombres de los modulos.
- Dentro de cada modulo tenemos al controlador como archivo, donde cada controlador tiene en una carpeta llamada "_render_{nombre_del_controlador}" todas las secciones del controlador

Ejem : 
Modulo : Postulante
Controlador : home

1. dentro de stylus/modules/

-- fonts.styl
-- ie.styl
-- rems.py
-- all/
-- postulante/

2. Dentro de la carpeta postulante
 - Siempre va haber una carpeta _render_all/
 - Tambien se colocaria la carpeta del controllador en este caso seria, _render_home/
 - Tambien habria el fichero llamado con el nombre del controlador en este caso seria home.styl, dentro de este fichero
   se requeriria config y mixins y se importaria su folder en este caso se llamaria _render_home.
   
-- home.styl
-- _render_all/
-- _render_home/

3. Dentro de la carpeta _render_home van todas las secciones divididos por ficheros

Si el html es asi:
<section class="search_home_box"></section>

Entonces se tendria un fichero llamado : search_home_box.styl

Dentro del archivo search_home_box.styl, irian todos los estilos solo de esta sección

Cuando se hace "gulp fonts" hace lo siguiente:

dentro de frontend/neoauto3/static/fonts/_template

Se encuentra un archivo de configuracion fonts.styl, este archivo lee todas las fuentes
que se encuentan en sus carpetas.

frontend/neoauto3/static/fonts/

-- /_template
-- /evogria-italc (fuente)
-- /evogria-rgl   (fuente)
-- /iconFonts     (fuente)
-- Todas las fuentes agrega a esta carpeta

gulp fonts genera un archivo fonts.styl con todas las fuentes y lo copia en:

frontend/neoauto3/stylus/modules/fonts.styl

Despues para generar el css se ejecuta el gulp styles, lo va a generar en la carpeta:

public/static/neoauto3/css/modules/fonts.css


/frontend/neoauto3/stylus/modules/all/_render_all/layout.styl
/public/static/neoauto3/css/modules/all/all.css
