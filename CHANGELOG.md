### 1.0.5 (2015-05-19)


### 1.0.4 (2015-05-19)


### 1.0.3 (2015-05-19)


#### Bug Fixes

* **AvisosDestacados:** Se cambio el zindex ([81f4de42](https://github.com/frontend-labs/flux.git/commit/81f4de4279b9de526c01d0f9c2b441c69653629d))
* **auth:**
  * Corrigiendo controller recover y agregando jade v.2.0 ([3995265a](https://github.com/frontend-labs/flux.git/commit/3995265a5ea7f4b196e2f8da4ce188d6244fbb77))
  * Corrigiendo controller recover y agregando jade ([ed4e1206](https://github.com/frontend-labs/flux.git/commit/ed4e1206c72684a24348a970c84c1f8f070e9208))
  * Corrigiendo controller recover y agregando jade ([9eb9cc91](https://github.com/frontend-labs/flux.git/commit/9eb9cc91477cc11c72df4591cdd7391b2412350c))
  * Corregio duplicado de funcion v.2.0 ([9badec2a](https://github.com/frontend-labs/flux.git/commit/9badec2a304eef1ff7385eea81fc94c788834dcd))
  * Corregio duplicado de funcion ([6c3b53e2](https://github.com/frontend-labs/flux.git/commit/6c3b53e25ad25e68720d319ba7adfb17ed3bbd09))
* **blog:**
  * hover para ie8 ([e398334a](https://github.com/frontend-labs/flux.git/commit/e398334a049923e577e934af7bf7b206ae1ee699))
  * se corrigio flechas para ie ([b34ba95d](https://github.com/frontend-labs/flux.git/commit/b34ba95d87f932f7ce79ec53e54d119befe67c5a))
  * se reubico las flechas del blof ([0314ff74](https://github.com/frontend-labs/flux.git/commit/0314ff7454c3018e361121c8cb458fc3a90cc4f4))
  * se coloco un max-height a las imagenes ([de1ed055](https://github.com/frontend-labs/flux.git/commit/de1ed055c72f75bb49583bc99c29c55acd88a080))
* **blox:** se quito controls a slider ([6cbf87ad](https://github.com/frontend-labs/flux.git/commit/6cbf87ad2408cc51c607172cbcaf436b59fc6efb))
* **boxLogin:** colocando modal a mobile ([1dcb94b4](https://github.com/frontend-labs/flux.git/commit/1dcb94b450abb78cf6e681ba1b7a6facaf5a2ff8))
* **boxModal:** se arreglo width undefined para ie8 ([05e41c50](https://github.com/frontend-labs/flux.git/commit/05e41c5078fa79a2f442eeb1ac5ef42fc6f9f3ee))
* **buscar vehiculos:** se coloco background opacity crossbroser ie ([f077cef8](https://github.com/frontend-labs/flux.git/commit/f077cef8488d575f23670f606b1ff3ad93a08a7e))
* **buscarVehiculos:** se arreglo link de nuevos y motos ([1ac6b24d](https://github.com/frontend-labs/flux.git/commit/1ac6b24d8ebf2fda27799bf79795ed82651baf92))
* **featModal:** fix precarga de iconos IE ([f68388f9](https://github.com/frontend-labs/flux.git/commit/f68388f953c6ad4d0280ff39199fe80e97abd7ae))
* **flux:**
  * Se arreglo modules.js ([06da32b3](https://github.com/frontend-labs/flux.git/commit/06da32b33a2e74a24f052b0e64859308c5febaf7))
  * delete files .orig ([748f1d38](https://github.com/frontend-labs/flux.git/commit/748f1d3883a30ad12a0d539e087a6f36ea2661db))
* **footer:**
  * Se agrego capitalize a los links ([85a509df](https://github.com/frontend-labs/flux.git/commit/85a509df6897cd0684cb43df41e8ab3dbd41c310))
  * se cambio identacion ([f1047286](https://github.com/frontend-labs/flux.git/commit/f1047286b93b29a488eb92a5fb02342f3a6db183))
* **headTouch:** se agrego eventos touch para menu ([d297cbfa](https://github.com/frontend-labs/flux.git/commit/d297cbfab0c89d0de72fb08d53579552a2982c87))
* **home:**
  * Se coloco mas espacio entre el precio y el km ([397ce254](https://github.com/frontend-labs/flux.git/commit/397ce254cb76aadbeb05207e038533d3d6a90539))
  * se cambio la identacion ([bed5e853](https://github.com/frontend-labs/flux.git/commit/bed5e8535a5f92a3aa2426b96f1fd71c169999a0))
  * se comento var_dump de la vista home ([dd539a0f](https://github.com/frontend-labs/flux.git/commit/dd539a0f839f5cd123d0e81cbbf5a99f04a3385b))
* **icons:** se cambio de iconos ([b2dc9f94](https://github.com/frontend-labs/flux.git/commit/b2dc9f94f15c50921c987b6e1a8108626f2e3719))
* **info:** se agrego margin al button informate aqui ([344d7981](https://github.com/frontend-labs/flux.git/commit/344d7981ceed1fae33a46f4ab73f8f15c9bb2f81))
* **lazyload:** se agrego lazyload a todo el sitio ([a3a8e85e](https://github.com/frontend-labs/flux.git/commit/a3a8e85ecc3933ce2e71d0490c9d80eabf434d87))
* **loginModal:** se borro comentarios ([da9a2d80](https://github.com/frontend-labs/flux.git/commit/da9a2d80638fedcfc516a9c442a23f9cb2cdd221))
* **modal:** fix title login ([832cad59](https://github.com/frontend-labs/flux.git/commit/832cad59fda032e3929bf852d2998f253e97250e))
* **modal login:** se coloco modal al login ([54d227c4](https://github.com/frontend-labs/flux.git/commit/54d227c4ea7e9869f0ddef7edbfdcddb5895f20c))
* **modalLogin:** se quito comentario de consola ([43a0c1e7](https://github.com/frontend-labs/flux.git/commit/43a0c1e709f2220668b007a79340d8cfa2504777))
* **publicacion-paso3:** Problema del alert, en subida de imagenes ([b5a4cfec](https://github.com/frontend-labs/flux.git/commit/b5a4cfece975db578527decd295081c191c4a81a))
* **sliderBlog:** se coloco para tablet slide de 1 en 1 ([996b941b](https://github.com/frontend-labs/flux.git/commit/996b941bfeb5ce6ecf337ff12399ae1353aa6e3e))
* **sliderBlogHome:** se agrego precarga de imagenes del blog ([dd009321](https://github.com/frontend-labs/flux.git/commit/dd009321552c6e43cadd2ec936a41c9e4d8e8d1b))
* **sticky:**
  * se mensaje de respuesta del aja ([38b5c96d](https://github.com/frontend-labs/flux.git/commit/38b5c96d5e1d3cfde9fe9378bf59d028343315e2))
  * se quito transparencia para ie8 ([b9b6851d](https://github.com/frontend-labs/flux.git/commit/b9b6851d75fd26e18c9792822544e14e4c14a86d))
  * Se corrigio el checkbox del formulario ([573f7894](https://github.com/frontend-labs/flux.git/commit/573f789481bfb8512853c0fec97eb921908da6e2))
* **stickyHome:** Se coloco la url token ([1437c3e7](https://github.com/frontend-labs/flux.git/commit/1437c3e74f4b2b2b82f4252abed442cde12b85a3))
* **touch:**
  * se quito for-in por $.each ([a474c0fd](https://github.com/frontend-labs/flux.git/commit/a474c0fdd73a250497806c5deab557d4a6a57c8c))
  * se cambio el elemento del padre ([26760973](https://github.com/frontend-labs/flux.git/commit/267609730cd0943028210fc28dee6f57da5c9295))
  * menu desplegable de servicios ([307501c1](https://github.com/frontend-labs/flux.git/commit/307501c1afa2d79566162657b7bdd5de1b15acd7))


#### Features

* **Aviso:**
  * implements function get images S3 ([159c947a](https://github.com/frontend-labs/flux.git/commit/159c947a45e674b364919d03e517fc220b2a9ba0))
  * implements function get images S3 ([acc9eed8](https://github.com/frontend-labs/flux.git/commit/acc9eed8aff1791de2f9a98967c6a52ad249b05c))
  * implements function delete image S3 ([fc621790](https://github.com/frontend-labs/flux.git/commit/fc6217901b260cff511c2626fb612d3d609c087b))
  * send json upload image S3 ([e434ae86](https://github.com/frontend-labs/flux.git/commit/e434ae869fb075a7ad094434693d44f1c994d582))
  * add model Paquete ([1b4eae61](https://github.com/frontend-labs/flux.git/commit/1b4eae619ac5393f781e557ce5d1cd16be5187f4))
  * modify configuration S3 ([6846b835](https://github.com/frontend-labs/flux.git/commit/6846b8355932ea0f57aee37311cc669200963a5a))
  * modify configuration S3 ([2ea632a1](https://github.com/frontend-labs/flux.git/commit/2ea632a19e33aebe99e889afe17b7d20f1eedc19))
  * Implement form image validations ([5f2443ec](https://github.com/frontend-labs/flux.git/commit/5f2443ec5783bd56bde001ff744fb97cd848363a))
* **HomeBuscaPor:**
  * se agrego ruta de imagenes para thumbs ([a1a3c5f4](https://github.com/frontend-labs/flux.git/commit/a1a3c5f4b799f3095d35fa8c0751a007f21b15b3))
  * se colocaron las nuevas url de s3 ([929c5074](https://github.com/frontend-labs/flux.git/commit/929c50740da56b15058a95e1614f8348cf5bf007))
* **SocialNetwoksHome:** se agrego links para redes sociales ([57b710ea](https://github.com/frontend-labs/flux.git/commit/57b710eadbb4ec771f53b9bc41deeddc7ba81b50))
* **Tracking:** Se añadio metodo para el manejo de tracking ([929caff2](https://github.com/frontend-labs/flux.git/commit/929caff2f15e31e292027d5fb1bc89c41c417ea8))
* **adsFeaturesHome:** Se coloco sombra a cada elemento ([1fcae30c](https://github.com/frontend-labs/flux.git/commit/1fcae30c34d176c2ea06d5476c10827ed7409d40))
* **anunciante:** Se creo modulo anunciante, reestructura de carpetas para anunciante en las vista ([9319c281](https://github.com/frontend-labs/flux.git/commit/9319c281885a3daed64ac503999d8848f885a7d8))
* **auth:**
  * Agregando libreria de facebook 3 ([3ef354e1](https://github.com/frontend-labs/flux.git/commit/3ef354e1c6ea8c5e643558d4bf8627c3661c29d5))
  * Agregando libreria de facebook 2 ([8e1a3769](https://github.com/frontend-labs/flux.git/commit/8e1a3769f9c289387c23cf527a9654382fb95da1))
  * Agregando libreria de facebook ([85ef2fcc](https://github.com/frontend-labs/flux.git/commit/85ef2fccddf1ed17607668e95b12f6f429174bd1))
  * Agregando libreria de facebook ([8a0b21d8](https://github.com/frontend-labs/flux.git/commit/8a0b21d8d69ee0fbb81623286c6bf0a90d69c578))
  * Agregando libreria de facebook ([6e3807b0](https://github.com/frontend-labs/flux.git/commit/6e3807b03f19cac76749825773e67635849625d4))
  * Agregando funciones registro facebook terminado ([629e75af](https://github.com/frontend-labs/flux.git/commit/629e75af5ac192888e087c7663d71c1f9e0e3674))
  * Agregando funciones registro facebook v.4.0 ([64427c87](https://github.com/frontend-labs/flux.git/commit/64427c873986c79ffa509362aebada7025cbf4fb))
  * Agregando al composer sdk facebook v.4.0 ([789ba79d](https://github.com/frontend-labs/flux.git/commit/789ba79dbe148b7274bedfa2d9d61261138856f9))
  * Agregando al composer sdk facebook v.3.0 ([7b51d7e5](https://github.com/frontend-labs/flux.git/commit/7b51d7e5d98f1c9da0c6e207d595d2179c97fcd6))
  * Agregando al composer sdk facebook v.2.0 ([fdd79e73](https://github.com/frontend-labs/flux.git/commit/fdd79e732dc55cf3ac68851df1593cd4f901eff5))
  * Agregando al composer sdk facebook ([f4856222](https://github.com/frontend-labs/flux.git/commit/f48562227ff6b0089075c157e3951caca70b8b53))
  * Recuperacion de password por ajax ([2720c8d6](https://github.com/frontend-labs/flux.git/commit/2720c8d624b906111aac42378cb1675c18ee42fc))
* **avisos:** cron desactivar avisos ([37aedeca](https://github.com/frontend-labs/flux.git/commit/37aedecaa7bc284f6132b172c7a229c0c882a1b6))
* **blog:**
  * se le agrego hover alos items ([de941329](https://github.com/frontend-labs/flux.git/commit/de941329ad7c4a9590e7a1d737f8b38b8872def1))
  * Se añadio maquetacion y funcionalidad responsive ([dfb04eb3](https://github.com/frontend-labs/flux.git/commit/dfb04eb32af4e4c90c5fb529d172b3dc18816098))
  * Se maqueto la seccion blog responsive ([d28e8e89](https://github.com/frontend-labs/flux.git/commit/d28e8e89e1d1823239ff74a0ce544b741740ff30))
* **buscaPor:** se maqueto la seccion con responsive ([09363da5](https://github.com/frontend-labs/flux.git/commit/09363da5f89304bb4754784de615674193837166))
* **default:** se mejoro tracking js ([51661e8f](https://github.com/frontend-labs/flux.git/commit/51661e8f34338c2f0142615777223dc1ee91857c))
* **favicon:** se añadio favicon ([5558e217](https://github.com/frontend-labs/flux.git/commit/5558e217b9262790a24ca2fb99186be0547011e3))
* **flux:**
  * textarea control de caracteres ([f5f8d2d6](https://github.com/frontend-labs/flux.git/commit/f5f8d2d6e96c71e327cfe317e68d99307c4e4e18))
  * control de caracteres ([fa03bd2f](https://github.com/frontend-labs/flux.git/commit/fa03bd2f581b76587f863417022ba9a058381ec2))
  * interaccion javascript buscador avanzado, home>banner>search_advanced ([769e5ef6](https://github.com/frontend-labs/flux.git/commit/769e5ef6ec4b001f18560cd882e118bfb66ae8ec))
  * test ([9aecf070](https://github.com/frontend-labs/flux.git/commit/9aecf070da4a753b4db5b33ee297996632c59a4f))
  * buscador avanzado en proceso ([0a5953cd](https://github.com/frontend-labs/flux.git/commit/0a5953cd1f03bce28cf7587e5d85e642f2f1d39c))
  * se elimino .btn_gray y se colocaron configs para publication y se creo un solo p ([3033345e](https://github.com/frontend-labs/flux.git/commit/3033345e4022dcd95bc39481f5dd8d32b38c7c40))
  * Se elimino index.jade de jadeflux ([b31734de](https://github.com/frontend-labs/flux.git/commit/b31734dedfa17ab013bd11fba63216879aa17b59))
  * Se agrego Demo, con configuraciones para back de jade ([fdc95c47](https://github.com/frontend-labs/flux.git/commit/fdc95c47ba168c90b9a51d90de2509ef46e2376d))
  * se agrego demo de ajax ([2078d1e6](https://github.com/frontend-labs/flux.git/commit/2078d1e607dd4102eaa17613df63fa9f520521bf))
  * se colocaron variables de configuracion en el site ([9f338024](https://github.com/frontend-labs/flux.git/commit/9f3380247ee3c147316a86e9a06cc1f7b7a3e7d7))
  * edicion css header y html header ([1149b66a](https://github.com/frontend-labs/flux.git/commit/1149b66a0a9f9682194d0510af577312a6571a38))
  * cambio en el page controller ([a62bed10](https://github.com/frontend-labs/flux.git/commit/a62bed1074cb42b71e980f5087b47548356dfbbd))
  * se omitio la carpeta node_modules ([ac5de82d](https://github.com/frontend-labs/flux.git/commit/ac5de82d334ce252b680d9676c2413ee00a20aa6))
  * Se añadio la carpeta frontend ([0c107171](https://github.com/frontend-labs/flux.git/commit/0c1071715bc0f2fe45fffb5b6cf97fdbf51fc9e2))
* **footer:**
  * se añadio footer para mobile ([572ae5b9](https://github.com/frontend-labs/flux.git/commit/572ae5b9eaef2a71d559c60445c8d386dd47f15c))
  * se mejoro responsive para tablet ([74865fc4](https://github.com/frontend-labs/flux.git/commit/74865fc453d96710b3baffde9e7e7fe74cab93c7))
  * Se maqueto el footer responsive ([a4f8904c](https://github.com/frontend-labs/flux.git/commit/a4f8904cd2199ee8ada62e91c30e25d6302cba84))
  * se agrego html para el footer ([778f6362](https://github.com/frontend-labs/flux.git/commit/778f636268d4a18ce4379d1fe808cb09d9765ab1))
* **fuentes:** se añadio fuentes ([cfe4d7cb](https://github.com/frontend-labs/flux.git/commit/cfe4d7cba480e9a9fdd27dcffc2ebe7da9359b1f))
* **headSearch:** se agrego icons list view ([e0b1ffa9](https://github.com/frontend-labs/flux.git/commit/e0b1ffa9a74dcac4b11a37eeefee27f828610531))
* **home:**
  * se agrego mixins para controlar los bloques y reutilizar codigo ([e74cb1ad](https://github.com/frontend-labs/flux.git/commit/e74cb1ad5a88100f0b6c2d707e3f3be7b686dc26))
  * submenu en desktop, tablet, mobile funcionando correctamente ([993edba8](https://github.com/frontend-labs/flux.git/commit/993edba890847aaf300e27693a39444d460f0920))
  * se agrego submenu en los links de header desktop y tablet ([741f549e](https://github.com/frontend-labs/flux.git/commit/741f549e385ff10280ec4af4366170dc2a899135))
  * buscador rapido terminado ([926d6187](https://github.com/frontend-labs/flux.git/commit/926d61874ae64a613c20e7249177237034e38520))
  * funcionalidad buscador rapido 30%, estoy haciendo un commit por que necesito tra ([1c701115](https://github.com/frontend-labs/flux.git/commit/1c7011152d9c7863c1dbaf3c4d3e18f1b19e28ef))
  * se maqueto contenido de autos usados mas optimizacion de tabs ([4666e05e](https://github.com/frontend-labs/flux.git/commit/4666e05eee7d5ab07a3a9778c5633082828d52cf))
  * maquetacion del contenido modelo ([8da650d2](https://github.com/frontend-labs/flux.git/commit/8da650d215b0b8a89862365436d360e0ef5923a3))
* **library:**
  * modificando libreria upload ([411b486d](https://github.com/frontend-labs/flux.git/commit/411b486dccd43d87a4c19596bd44ac9c842a308f))
  * agregando libreria upload ([b11a2695](https://github.com/frontend-labs/flux.git/commit/b11a26956ed42c86ff2babd4099df868d0ba765e))
* **login:** validacion de usuario ([3689855e](https://github.com/frontend-labs/flux.git/commit/3689855e7a54ad897fc527513be8da889c919451))
* **loginPopUp:** maquetacion completa ([5e0f6f9f](https://github.com/frontend-labs/flux.git/commit/5e0f6f9f6b536eff1638b1ec46503eb71d64e373))
* **modalLogin:** se coloco funcionalidad al modal para mobile ([a320b3c9](https://github.com/frontend-labs/flux.git/commit/a320b3c9a46831a8136a9ce22493ff54fa83d4a6))
* **neoauto2:**
  * implement  filters km, year, price webservice ([58e86779](https://github.com/frontend-labs/flux.git/commit/58e867798267c3af1e55b6b43e87939480fddf43))
  * implement  filters km, year, price webservice ([75f6f2b2](https://github.com/frontend-labs/flux.git/commit/75f6f2b2711aa2531d1de6b53d22eccc1e320d6f))
  * implement  filters km, year, price webservice ([85cb4079](https://github.com/frontend-labs/flux.git/commit/85cb4079caf12edea2ee81464ac956091ff48fe9))
  * se añadio default.js ([26d1bf6b](https://github.com/frontend-labs/flux.git/commit/26d1bf6bd1610131e220dfa8030ff5c9c2c5d3a4))
* **new_home:** avanze de modificacion del home todo el header y buscador avanzado en un 60% ([e3a7cddb](https://github.com/frontend-labs/flux.git/commit/e3a7cddb9bdea557f0c1f672db50c4a028b636ef))
* **popupVisa:** se agrego funcionalidad para popup visa ([b420529a](https://github.com/frontend-labs/flux.git/commit/b420529abe041f29415564ea7c8f52b9318d7081))
* **publicacion:**
  * modelo UsuarioEmpresa creacion ([560f1017](https://github.com/frontend-labs/flux.git/commit/560f1017fa9c2677d76f6036eb0263278b13759c))
  * modelo Ente creacion ([a26be059](https://github.com/frontend-labs/flux.git/commit/a26be059f08c30e58510707291b5fed1b3bebbe0))
  * creando funcion subir imagenes s3 ([9f13ea36](https://github.com/frontend-labs/flux.git/commit/9f13ea369707a47975690b5d5589a4f107a6c343))
  * creo modelo auto y foto ([53523861](https://github.com/frontend-labs/flux.git/commit/53523861aed6c9309e03a0a44ac7a197c8663797))
* **resultsSearch:** añadiendo comportamiento responsive ([aac8e2f7](https://github.com/frontend-labs/flux.git/commit/aac8e2f728ee579bea23bae82a103de44c8ebbed))
* **search:**
  * filtro funcionalidad slideToggle ([5c29322e](https://github.com/frontend-labs/flux.git/commit/5c29322ecb22219207f671d87cbca493cd8d2a27))
  * modulo y stylus de base ([72eefcc0](https://github.com/frontend-labs/flux.git/commit/72eefcc073fba40c76874fcccf7278d3dfb7ad05))
  * se borro un modulo antiguo _searchold ([17d00611](https://github.com/frontend-labs/flux.git/commit/17d00611e461f56fd8f5796e936ca46bfcc5bfe5))
  * Agregando listaTipoAuto ([6bf0a117](https://github.com/frontend-labs/flux.git/commit/6bf0a117f6958dc88e3bc292f5ccc0cdc0844c15))
  * Agregando listaTipoAuto ([270de629](https://github.com/frontend-labs/flux.git/commit/270de629f646e18a8d741b438c8aca233ea37ac4))
* **site:** No se hace pinch en mobile, se coloco el selectivizr correcto para ie, se agrego ([4116ad8c](https://github.com/frontend-labs/flux.git/commit/4116ad8ceeeac0b545e086303701b9aa58b20e7d))
* **social_networks:** se agrego fade a los iconos ([bba0bf2d](https://github.com/frontend-labs/flux.git/commit/bba0bf2d84e57c595c562088b994ea5c19f4cd70))
* **solar:** se comento conexion de solar ([2a2e0695](https://github.com/frontend-labs/flux.git/commit/2a2e06952b741daf215e2cc8820e60c5dcd50a57))
* **sticky:**
  * Nuevo estilo del stiky ([b7473782](https://github.com/frontend-labs/flux.git/commit/b7473782b142e98d78239a9a0c335efb206c9d37))
  * se maqueto con funcionalidad el sticky del home ([96eac067](https://github.com/frontend-labs/flux.git/commit/96eac067cb24ce0409612f9a529e1df81277f4ba))
* **stickyHome:**
  * se coloco el message de respuesta del ajax de back ([be860d49](https://github.com/frontend-labs/flux.git/commit/be860d4969b40cae468b619388768961533b7254))
  * se agrego loader al token ([0835e170](https://github.com/frontend-labs/flux.git/commit/0835e170d0e4f31d8750b48046fd7a4a34f37ebd))
* **touch:** se añadio modulo de touchend por separado ([16a13423](https://github.com/frontend-labs/flux.git/commit/16a13423f92da18c8b6859fe0e95b9d7e043adec))
* **tracking:**
  * se agrego tracking para registro ([8cacacf4](https://github.com/frontend-labs/flux.git/commit/8cacacf4f67a7cf11d3ad9cdfecaf5b07691d384))
  * se agrego tracking para menu principal, menu top, footer, se mejoro el plugin de ([b6f3dde8](https://github.com/frontend-labs/flux.git/commit/b6f3dde8ac46bcd8966df62a96be9d9387490086))
* **yoson:** se actualizo la version del yoson en bower ([52221fa9](https://github.com/frontend-labs/flux.git/commit/52221fa96b64c078267012391c81f67fc927f801))


### 1.0.2 (2014-11-13)


#### Bug Fixes

* **jadeflux:** Se corrige error compilacion en versiones de jade > 1.3.1. ([94b0428e](https://github.com/frontend-labs/flux.git/commit/94b0428e88fee58ae7b510a586517cdc2706d98f))


#### Features

* **.gitignore:** Quitando las fonts compiladas del repositorio. ([08d321d0](https://github.com/frontend-labs/flux.git/commit/08d321d0bf8e8a11bdfb6d8a4c2b9ab1b968d6d4))
* **flux:** Actualizando Flux a su version 1.0.2. ([7a9cc7ba](https://github.com/frontend-labs/flux.git/commit/7a9cc7ba4ec8f92f905312446ce6744acd8744d8))
* **gulp:**
  * Agregando tareas para generar fuente de letras automatico. ([fc1946d4](https://github.com/frontend-labs/flux.git/commit/fc1946d4510b6c407c1ebd83397eecb65d74ed11))
  * Agregando tareas para generar fuente de iconos automatico. ([f8a61257](https://github.com/frontend-labs/flux.git/commit/f8a612579963fe8b679c241cc5bbc8b00026458a))


### 1.0.3 (2014-11-13)


#### Bug Fixes

* **jadeflux:** Se corrige error compilacion en versiones de jade > 1.3.1. ([94b0428e](https://github.com/jansanchez/flux.git/commit/94b0428e88fee58ae7b510a586517cdc2706d98f))


#### Features

* **flux:** Actualizando Flux a su version 1.0.2. ([7a9cc7ba](https://github.com/jansanchez/flux.git/commit/7a9cc7ba4ec8f92f905312446ce6744acd8744d8))
* **gulp:**
  * Agregando tareas para generar fuente de letras automatico. ([fc1946d4](https://github.com/jansanchez/flux.git/commit/fc1946d4510b6c407c1ebd83397eecb65d74ed11))
  * Agregando tareas para generar fuente de iconos automatico. ([f8a61257](https://github.com/jansanchez/flux.git/commit/f8a612579963fe8b679c241cc5bbc8b00026458a))


### 1.0.2 (2014-11-13)


#### Bug Fixes

* **jadeflux:** Se corrige error compilacion en versiones de jade > 1.3.1. ([94b0428e](https://github.com/jansanchez/flux.git/commit/94b0428e88fee58ae7b510a586517cdc2706d98f))


#### Features

* **flux:** Actualizando Flux a su version 1.0.2. ([7a9cc7ba](https://github.com/jansanchez/flux.git/commit/7a9cc7ba4ec8f92f905312446ce6744acd8744d8))
* **gulp:**
  * Agregando tareas para generar fuente de letras automatico. ([fc1946d4](https://github.com/jansanchez/flux.git/commit/fc1946d4510b6c407c1ebd83397eecb65d74ed11))
  * Agregando tareas para generar fuente de iconos automatico. ([f8a61257](https://github.com/jansanchez/flux.git/commit/f8a612579963fe8b679c241cc5bbc8b00026458a))


### 1.0.1 (2014-11-06)


#### Features

* **gulp:** Agregando tareas para bump y changelog automaticos. ([8887b943](https://github.com/frontend-labs/flux.git/commit/8887b943aa47446bac374c084c6a885ef8b6790b))
* **package.json:**
  * Agregando gulp-css-url-versioner como dependencia. ([88584caa](https://github.com/frontend-labs/flux.git/commit/88584caa3f66180c5652949c1f5a681d68eb7936))
  * Agregando nuevas dependencias. ([090b826c](https://github.com/frontend-labs/flux.git/commit/090b826c9472f15dbe5b93ff186b1efc2924ed7a))


